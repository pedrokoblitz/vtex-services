package vtex


type Address struct {
	AddressType    string      `json:"addressType"`
	ReceiverName   string      `json:"receiverName"`
	AddressID      string      `json:"addressId"`
	PostalCode     string      `json:"postalCode"`
	City           string      `json:"city"`
	State          string      `json:"state"`
	Country        string      `json:"country"`
	Street         string      `json:"street"`
	Number         string      `json:"number"`
	Neighborhood   string      `json:"neighborhood"`
	Complement     string      `json:"complement"`
	Reference      string      `json:"reference"`
	GeoCoordinates []float64   `json:"geoCoordinates"`
	AddressQuery   string      `json:"addressQuery"`
}


type ClientProfileData struct {
	CorporateDocument        string `json:"corporateDocument"`
	CorporateName            string `json:"corporateName"`
	CorporatePhone           string `json:"corporatePhone"`
	CustomerClass            string `json:"customerClass"`
	Document                 string      `json:"document"`
	DocumentType             string      `json:"documentType"`
	Email                    string      `json:"email"`
	FirstName                string      `json:"firstName"`
	IsCorporate              bool        `json:"isCorporate"`
	LastName                 string      `json:"lastName"`
	Phone                    string      `json:"phone"`
	ProfileCompleteOnLoading bool        `json:"profileCompleteOnLoading"`
	ProfileErrorOnLoading    bool        `json:"profileErrorOnLoading"`
	StateInscription         string      `json:"stateInscription"`
	TradeName                string `json:"tradeName"`
}

type ClientPreferencesData  struct {
	Locale          string `json:"locale"`
	OptinNewsLetter bool   `json:"optinNewsLetter"`
}

type ItemMeta struct {
	AssemblyOptions []interface{} `json:"assemblyOptions"`
	DetailURL       string        `json:"detailUrl"`
	Ean             string        `json:"ean"`
	ID              string        `json:"id"`
	ImageURL        string        `json:"imageUrl"`
	Name            string        `json:"name"`
	ProductID       string        `json:"productId"`
	RefID           string        `json:"refId"`
	Seller          string        `json:"seller"`
	SkuName         string        `json:"skuName"`
}

type AdditionalInfo struct {
	BrandID        string      `json:"brandId"`
	BrandName      string      `json:"brandName"`
	OfferingInfo   interface{} `json:"offeringInfo"`
	OfferingType   interface{} `json:"offeringType"`
	OfferingTypeID interface{} `json:"offeringTypeId"`
}

type Offering struct {
	AllowGiftMessage    bool          `json:"allowGiftMessage"`
	AttachmentOfferings []interface{} `json:"attachmentOfferings"`
	ID                  string        `json:"id"`
	Name                string        `json:"name"`
	Price               int64         `json:"price"`
	Type                string        `json:"type"`
}

type ItemData struct {
	AdditionalInfo AdditionalInfo `json:"additionalInfo"`
	Assemblies          []interface{} `json:"assemblies"`
	AttachmentOfferings []interface{} `json:"attachmentOfferings"`
	Attachments         []interface{} `json:"attachments"`
	Availability        string        `json:"availability"`
	BundleItems         []interface{} `json:"bundleItems"`
	Components          []interface{} `json:"components"`
	DetailURL           string        `json:"detailUrl"`
	Ean                 string        `json:"ean"`
	ID                  string        `json:"id"`
	ImageURL            string        `json:"imageUrl"`
	IsGift              bool          `json:"isGift"`
	ListPrice           int64         `json:"listPrice"`
	ManualPrice         interface{}   `json:"manualPrice"`
	MeasurementUnit     string        `json:"measurementUnit"`
	ModalType           interface{}   `json:"modalType"`
	Name                string        `json:"name"`
	Offerings           []Offering `json:"offerings"`
	ParentAssemblyBinding interface{}   `json:"parentAssemblyBinding"`
	ParentItemIndex       interface{}   `json:"parentItemIndex"`
	PreSaleDate           interface{}   `json:"preSaleDate"`
	Price                 int64         `json:"price"`
	PriceTags             []interface{} `json:"priceTags"`
	PriceValidUntil       string        `json:"priceValidUntil"`
	ProductCategories     map[string]interface{} `json:"productCategories"`
	ProductCategoryIds string   `json:"productCategoryIds"`
	ProductID          string   `json:"productId"`
	ProductRefID       string   `json:"productRefId"`
	Quantity           int64    `json:"quantity"`
	RefID              string   `json:"refId"`
	RewardValue        int64    `json:"rewardValue"`
	Seller             string   `json:"seller"`
	SellerChain        []string `json:"sellerChain"`
	SellingPrice       int64    `json:"sellingPrice"`
	SkuName            string   `json:"skuName"`
	Tax                int64    `json:"tax"`
	UniqueID           string   `json:"uniqueId"`
	UnitMultiplier     int64    `json:"unitMultiplier"`
}

type SellerMerchantInstallment struct {
	Count           int64  `json:"count"`
	HasInterestRate bool   `json:"hasInterestRate"`
	ID              string `json:"id"`
	InterestRate    int64  `json:"interestRate"`
	Total           int64  `json:"total"`
	Value           int64  `json:"value"`
}

type Installment struct {
	Count                      int64 `json:"count"`
	HasInterestRate            bool  `json:"hasInterestRate"`
	InterestRate               int64 `json:"interestRate"`
	SellerMerchantInstallments []SellerMerchantInstallment `json:"sellerMerchantInstallments"`
	Total int64 `json:"total"`
	Value int64 `json:"value"`
}

type InstallmentOption struct {
	Bin          interface{} `json:"bin"`
	Installments []Installment `json:"installments"`
	PaymentGroupName interface{} `json:"paymentGroupName"`
	PaymentName      interface{} `json:"paymentName"`
	PaymentSystem    string      `json:"paymentSystem"`
	Value            int64       `json:"value"`
}

type Totalizer struct {
	ID    string `json:"id"`
	Name  string `json:"name"`
	Value int64  `json:"value"`
}

type BusinessHours struct {
	ClosingTime string `json:"ClosingTime"`
	DayOfWeek   int64  `json:"DayOfWeek"`
	OpeningTime string `json:"OpeningTime"`
}

type CurrencyFormatInfo struct {
	CurrencyDecimalDigits    int64  `json:"currencyDecimalDigits"`
	CurrencyDecimalSeparator string `json:"currencyDecimalSeparator"`
	CurrencyGroupSeparator   string `json:"currencyGroupSeparator"`
	CurrencyGroupSize        int64  `json:"currencyGroupSize"`
	StartsWithCurrencySymbol bool   `json:"startsWithCurrencySymbol"`
}

type StorePreferencesData struct {
	CountryCode        string `json:"countryCode"`
	CurrencyCode       string `json:"currencyCode"`
	CurrencyFormatInfo CurrencyFormatInfo `json:"currencyFormatInfo"`
	CurrencyLocale int64  `json:"currencyLocale"`
	CurrencySymbol string `json:"currencySymbol"`
	SaveUserData   bool   `json:"saveUserData"`
	TimeZone       string `json:"timeZone"`
}

type PickupPoint struct {
	AdditionalInfo string `json:"additionalInfo"`
	Address Address `json:"address"`
	BusinessHours []BusinessHours `json:"businessHours"`
	FriendlyName string `json:"friendlyName"`
	ID           string `json:"id"`
}

type PickupStoreInfo struct {
	AdditionalInfo string `json:"additionalInfo"`
	Address Address`json:"address"`
	DockID        string `json:"dockId"`
	FriendlyName  string `json:"friendlyName"`
	IsPickupStore bool   `json:"isPickupStore"`
}

type DeliveryId struct {
	CourierID   string `json:"courierId"`
	CourierName string `json:"courierName"`
	DockID      string `json:"dockId"`
	Quantity    int64  `json:"quantity"`
	WarehouseID string `json:"warehouseId"`
}

type RatesAndBenefitsData struct {
	RateAndBenefitsIdentifiers []interface{} `json:"rateAndBenefitsIdentifiers"`
	Teaser                     []interface{} `json:"teaser"`
}

type PaymentValidator struct {
	CardCodeMask      string  `json:"cardCodeMask"`
	CardCodeRegex     string  `json:"cardCodeRegex"`
	Mask              string  `json:"mask"`
	Regex             string  `json:"regex"`
	UseBillingAddress bool    `json:"useBillingAddress"`
	UseCardHolderName bool    `json:"useCardHolderName"`
	UseCvv            bool    `json:"useCvv"`
	UseExpirationDate bool    `json:"useExpirationDate"`
	Weights           []int64 `json:"weights"`
}

type MerchantSellerPayment struct {
	ID               string `json:"id"`
	InstallmentValue int64  `json:"installmentValue"`
	Installments     int64  `json:"installments"`
	InterestRate     int64  `json:"interestRate"`
	ReferenceValue   int64  `json:"referenceValue"`
	Value            int64  `json:"value"`
}

type Payment struct {
	AccountID              interface{} `json:"accountId"`
	Bin                    interface{} `json:"bin"`
	Installments           int64       `json:"installments"`
	MerchantSellerPayments []MerchantSellerPayment `json:"merchantSellerPayments"`
	PaymentSystem  string      `json:"paymentSystem"`
	ReferenceValue int64       `json:"referenceValue"`
	TokenID        interface{} `json:"tokenId"`
	Value          int64       `json:"value"`
}

type PaymentData struct {
	AvailableAccounts  []interface{} `json:"availableAccounts"`
	AvailableTokens    []interface{} `json:"availableTokens"`
	GiftCardMessages   []interface{} `json:"giftCardMessages"`
	GiftCards          []interface{} `json:"giftCards"`
	InstallmentOptions []InstallmentOption `json:"installmentOptions"`
	PaymentSystems []PaymentSystem `json:"paymentSystems"`
	Payments []Payment `json:"payments"`
}

type PaymentSystem struct {
	AvailablePayments      interface{} `json:"availablePayments"`
	Description            interface{} `json:"description"`
	DueDate                string      `json:"dueDate"`
	GroupName              string      `json:"groupName"`
	ID                     int64       `json:"id"`
	IsCustom               bool        `json:"isCustom"`
	Name                   string      `json:"name"`
	RequiresAuthentication bool        `json:"requiresAuthentication"`
	RequiresDocument       bool        `json:"requiresDocument"`
	StringID               string      `json:"stringId"`
	Template               string      `json:"template"`
	Validator              PaymentValidator `json:"validator"`
}

type Seller struct {
	ID   string `json:"id"`
	Logo string `json:"logo"`
	Name string `json:"name"`
}

type Sla struct {
	AvailableDeliveryWindows []interface{} `json:"availableDeliveryWindows"`
	DeliveryChannel          string        `json:"deliveryChannel"`
	DeliveryIds              []DeliveryId `json:"deliveryIds"`
	DeliveryWindow  interface{} `json:"deliveryWindow"`
	ID              string      `json:"id"`
	ListPrice       int64       `json:"listPrice"`
	LockTTL         interface{} `json:"lockTTL"`
	Name            string      `json:"name"`
	PickupDistance  int64       `json:"pickupDistance"`
	PickupPointID   string      `json:"pickupPointId"`
	PickupStoreInfo PickupStoreInfo `json:"pickupStoreInfo"`
	PolygonName          interface{} `json:"polygonName"`
	Price                int64       `json:"price"`
	ShippingEstimate     string      `json:"shippingEstimate"`
	ShippingEstimateDate interface{} `json:"shippingEstimateDate"`
	Tax                  int64       `json:"tax"`
}

type DeliveryChannel struct {
	ID string `json:"id"`
}

type LogisticsInfo struct {
	AddressID        string `json:"addressId"`
	DeliveryChannels []DeliveryChannel `json:"deliveryChannels"`
	ItemID                  string   `json:"itemId"`
	ItemIndex               int64    `json:"itemIndex"`
	SelectedDeliveryChannel string   `json:"selectedDeliveryChannel"`
	SelectedSLA             string   `json:"selectedSla"`
	ShipsTo                 []string `json:"shipsTo"`
	Slas                    []Sla `json:"slas"`
}

type ShippingData struct {
	Address Address `json:"address"`
	AvailableAddresses []Address `json:"availableAddresses"`
	LogisticsInfo []LogisticsInfo `json:"logisticsInfo"`
	PickupPoints []PickupPoint `json:"pickupPoints"`
	SelectedAddresses []Address `json:"selectedAddresses"`
}

type Message struct {
	Code   interface{} `json:"code"`
	Fields struct{}    `json:"fields"`
	Status string      `json:"status"`
	Text   string      `json:"text"`
}

type ItemPurchaseCondition struct {
	ID          string        `json:"id"`
	ListPrice   int64         `json:"listPrice"`
	Price       int64         `json:"price"`
	Seller      string        `json:"seller"`
	SellerChain []string      `json:"sellerChain"`
	Slas        []interface{} `json:"slas"`
}

type Total struct {
	ID    string `json:"id"`
	Name  string `json:"name"`
	Value int64  `json:"value"`
}

type MerchantTransaction struct {
	ID           string `json:"id"`
	MerchantName string `json:"merchantName"`
	Payments     []Payment `json:"payments"`
	TransactionID string `json:"transactionId"`
}

type MarketingData struct {
	Coupon        interface{}   `json:"coupon"`
	MarketingTags []interface{} `json:"marketingTags"`
	UtmCampaign   interface{}   `json:"utmCampaign"`
	UtmMedium     interface{}   `json:"utmMedium"`
	UtmSource     interface{}   `json:"utmSource"`
	UtmiCampaign  interface{}   `json:"utmiCampaign"`
	UtmiPart      interface{}   `json:"utmiPart"`
	Utmipage      interface{}   `json:"utmipage"`
}

type VtexPaymentDataResponse struct {
	AllowManualPrice       bool        `json:"allowManualPrice"`
	CanEditData            bool        `json:"canEditData"`
	CheckedInPickupPointID interface{} `json:"checkedInPickupPointId"`
	ClientPreferencesData ClientPreferencesData `json:"clientPreferencesData"`
	ClientProfileData ClientProfileData`json:"clientProfileData"`
	CommercialConditionData interface{} `json:"commercialConditionData"`
	CustomData              interface{} `json:"customData"`
	GiftRegistryData        interface{} `json:"giftRegistryData"`
	HooksData               interface{} `json:"hooksData"`
	IgnoreProfileData       bool        `json:"ignoreProfileData"`
	InvoiceData             interface{} `json:"invoiceData"`
	IsCheckedIn             bool        `json:"isCheckedIn"`
	ItemMetadata            struct {
		Items []ItemMeta `json:"items"`
	} `json:"itemMetadata"`
	Items []ItemData `json:"items"`
	ItemsOrdination interface{} `json:"itemsOrdination"`
	LoggedIn        bool        `json:"loggedIn"`
	MarketingData MarketingData `json:"marketingData"`
	Messages      []interface{} `json:"messages"`
	OpenTextField interface{}   `json:"openTextField"`
	OrderFormID   string        `json:"orderFormId"`
	PaymentData   PaymentData `json:"paymentData"`
	RatesAndBenefitsData RatesAndBenefitsData`json:"ratesAndBenefitsData"`
	SalesChannel    string        `json:"salesChannel"`
	SelectableGifts []interface{} `json:"selectableGifts"`
	Sellers         []Seller `json:"sellers"`
	ShippingData ShippingData `json:"shippingData"`
	StoreID              interface{} `json:"storeId"`
	StorePreferencesData StorePreferencesData `json:"storePreferencesData"`
	SubscriptionData interface{} `json:"subscriptionData"`
	Totalizers       []Totalizer `json:"totalizers"`
	UserProfileID interface{} `json:"userProfileId"`
	UserType      interface{} `json:"userType"`
	Value         int64       `json:"value"`
}


type VtexShippingDataResponse struct {
	ClientProfileData ClientProfileData `json:"clientProfileData"`
	ClientPreferencesData  ClientPreferencesData `json:"clientPreferencesData"`
	AllowManualPrice       bool        `json:"allowManualPrice"`
	CanEditData            bool        `json:"canEditData"`
	CheckedInPickupPointID interface{} `json:"checkedInPickupPointId"`
	CommercialConditionData interface{} `json:"commercialConditionData"`
	CustomData              interface{} `json:"customData"`
	GiftRegistryData        interface{} `json:"giftRegistryData"`
	HooksData               interface{} `json:"hooksData"`
	IgnoreProfileData       bool        `json:"ignoreProfileData"`
	InvoiceData             interface{} `json:"invoiceData"`
	IsCheckedIn             bool        `json:"isCheckedIn"`
	ItemMetadata            struct {
		Items []ItemMeta `json:"items"`
	} `json:"itemMetadata"`
	Items []ItemData `json:"items"`
	ItemsOrdination interface{} `json:"itemsOrdination"`
	LoggedIn        bool        `json:"loggedIn"`
	MarketingData MarketingData`json:"marketingData"`
	Messages      []interface{} `json:"messages"`
	OpenTextField interface{}   `json:"openTextField"`
	OrderFormID   string        `json:"orderFormId"`
	PaymentData   PaymentData `json:"paymentData"`
	RatesAndBenefitsData RatesAndBenefitsData `json:"ratesAndBenefitsData"`
	SalesChannel    string        `json:"salesChannel"`
	SelectableGifts []interface{} `json:"selectableGifts"`
	Sellers         []Seller `json:"sellers"`
	ShippingData ShippingData `json:"shippingData"`
	StoreID              interface{} `json:"storeId"`
	StorePreferencesData StorePreferencesData `json:"storePreferencesData"`
	SubscriptionData interface{} `json:"subscriptionData"`
	Totalizers       []Totalizer `json:"totalizers"`
	UserProfileID interface{} `json:"userProfileId"`
	UserType      interface{} `json:"userType"`
	Value         int64       `json:"value"`
}

type VtexSimulationResponse struct {
	Country      string      `json:"country"`
	ItemMetadata interface{} `json:"itemMetadata"`
	Items        []ItemData `json:"items"`
	LogisticsInfo []LogisticsInfo `json:"logisticsInfo"`
	MarketingData MarketingData `json:"marketingData"`
	Messages []Message `json:"messages"`
	PaymentData PaymentData `json:"paymentData"`
	PickupPoints       []interface{} `json:"pickupPoints"`
	PostalCode         interface{}   `json:"postalCode"`
	PurchaseConditions struct {
		ItemPurchaseConditions []ItemPurchaseCondition `json:"itemPurchaseConditions"`
	} `json:"purchaseConditions"`
	RatesAndBenefitsData RatesAndBenefitsData `json:"ratesAndBenefitsData"`
	SelectableGifts  []interface{} `json:"selectableGifts"`
	SubscriptionData interface{}   `json:"subscriptionData"`
	Totals           []Total `json:"totals"`
}

type VtexOrderFormResponse struct {
	AllowManualPrice       bool        `json:"allowManualPrice"`
	CanEditData            bool        `json:"canEditData"`
	CheckedInPickupPointID interface{} `json:"checkedInPickupPointId"`
	ClientPreferencesData  struct {
		Locale          string      `json:"locale"`
		OptinNewsLetter interface{} `json:"optinNewsLetter"`
	} `json:"clientPreferencesData"`
	ClientProfileData       interface{} `json:"clientProfileData"`
	CommercialConditionData interface{} `json:"commercialConditionData"`
	CustomData              interface{} `json:"customData"`
	GiftRegistryData        interface{} `json:"giftRegistryData"`
	HooksData               interface{} `json:"hooksData"`
	IgnoreProfileData       bool        `json:"ignoreProfileData"`
	InvoiceData             interface{} `json:"invoiceData"`
	IsCheckedIn             bool        `json:"isCheckedIn"`
	ItemMetadata            struct {
		Items []ItemMeta `json:"itemMetadata"`
	}
	Items []ItemData `json:"items"`
	ItemsOrdination interface{} `json:"itemsOrdination"`
	LoggedIn        bool        `json:"loggedIn"`
	MarketingData MarketingData `json:"marketingData"`
	Messages      []Message `json:"messages"`
	OpenTextField interface{}   `json:"openTextField"`
	OrderFormID   string        `json:"orderFormId"`
	PaymentData  PaymentData `json:"paymentData"`
	RatesAndBenefitsData RatesAndBenefitsData `json:"ratesAndBenefitsData"`
	SalesChannel    string        `json:"salesChannel"`
	SelectableGifts []interface{} `json:"selectableGifts"`
	Sellers         []Seller `json:"sellers"`
	ShippingData ShippingData `json:"shippingData"`
	StoreID              interface{} `json:"storeId"`
	StorePreferencesData StorePreferencesData `json:"storePreferencesData"`
	SubscriptionData interface{} `json:"subscriptionData"`
	Totalizers       []Totalizer `json:"totalizers"`
	UserProfileID interface{} `json:"userProfileId"`
	UserType      interface{} `json:"userType"`
	Value         int64       `json:"value"`
}

type VtexAddItemResponse struct {
	AllowManualPrice       bool        `json:"allowManualPrice"`
	CanEditData            bool        `json:"canEditData"`
	CheckedInPickupPointID interface{} `json:"checkedInPickupPointId"`
	ClientPreferencesData ClientPreferencesData `json:"clientPreferencesData"`
	ClientProfileData       interface{} `json:"clientProfileData"`
	CommercialConditionData interface{} `json:"commercialConditionData"`
	CustomData              interface{} `json:"customData"`
	GiftRegistryData        interface{} `json:"giftRegistryData"`
	HooksData               interface{} `json:"hooksData"`
	IgnoreProfileData       bool        `json:"ignoreProfileData"`
	InvoiceData             interface{} `json:"invoiceData"`
	IsCheckedIn             bool        `json:"isCheckedIn"`
	ItemMetadata            struct {
		Items []ItemMeta `json:"items"`
	} `json:"itemMetadata"`
	Items []ItemData `json:"items"`
	ItemsOrdination interface{}   `json:"itemsOrdination"`
	LoggedIn        bool          `json:"loggedIn"`
	MarketingData   interface{}   `json:"marketingData"`
	Messages        []interface{} `json:"messages"`
	OpenTextField   interface{}   `json:"openTextField"`
	OrderFormID     string        `json:"orderFormId"`
	PaymentData     PaymentData `json:"paymentData"`
	RatesAndBenefitsData RatesAndBenefitsData `json:"ratesAndBenefitsData"`
	SalesChannel    string        `json:"salesChannel"`
	SelectableGifts []interface{} `json:"selectableGifts"`
	Sellers         []Seller `json:"sellers"`
	ShippingData ShippingData `json:"shippingData"`
	StoreID              interface{} `json:"storeId"`
	StorePreferencesData StorePreferencesData `json:"storePreferencesData"`
	SubscriptionData interface{} `json:"subscriptionData"`
	Totalizers       []Totalizer `json:"totalizers"`
	UserProfileID interface{} `json:"userProfileId"`
	UserType      interface{} `json:"userType"`
	Value         int64       `json:"value"`
}

type VtexCouponResponse struct {
	AllowManualPrice       bool        `json:"allowManualPrice"`
	CanEditData            bool        `json:"canEditData"`
	CheckedInPickupPointID interface{} `json:"checkedInPickupPointId"`
	ClientPreferencesData ClientPreferencesData `json:"clientPreferencesData"`
	ClientProfileData       interface{} `json:"clientProfileData"`
	CommercialConditionData interface{} `json:"commercialConditionData"`
	CustomData              interface{} `json:"customData"`
	GiftRegistryData        interface{} `json:"giftRegistryData"`
	HooksData               interface{} `json:"hooksData"`
	IgnoreProfileData       bool        `json:"ignoreProfileData"`
	InvoiceData             interface{} `json:"invoiceData"`
	IsCheckedIn             bool        `json:"isCheckedIn"`
	ItemMetadata            struct {
		Items []ItemMeta `json:"items"`
	} `json:"itemMetadata"`
	Items []ItemData `json:"items"`
	ItemsOrdination interface{} `json:"itemsOrdination"`
	LoggedIn        bool        `json:"loggedIn"`
	MarketingData MarketingData `json:"marketingData"`
	Messages []Message `json:"messages"`
	OpenTextField interface{} `json:"openTextField"`
	OrderFormID   string      `json:"orderFormId"`
	PaymentData PaymentData `json:"paymentData"`
	RatesAndBenefitsData RatesAndBenefitsData `json:"ratesAndBenefitsData"`
	SalesChannel    string        `json:"salesChannel"`
	SelectableGifts []interface{} `json:"selectableGifts"`
	Sellers         []Seller `json:"sellers"`
	ShippingData ShippingData `json:"shippingData"`
	StoreID              interface{} `json:"storeId"`
	StorePreferencesData StorePreferencesData `json:"storePreferencesData"`
	SubscriptionData interface{} `json:"subscriptionData"`
	Totalizers       []Totalizer `json:"totalizers"`
	UserProfileID interface{} `json:"userProfileId"`
	UserType      interface{} `json:"userType"`
	Value         int64       `json:"value"`
}

type VtexMktResponse struct {
	AllowManualPrice       bool        `json:"allowManualPrice"`
	CanEditData            bool        `json:"canEditData"`
	CheckedInPickupPointID interface{} `json:"checkedInPickupPointId"`
	ClientPreferencesData  ClientPreferencesData `json:"clientPreferencesData"`
	ClientProfileData       interface{} `json:"clientProfileData"`
	CommercialConditionData interface{} `json:"commercialConditionData"`
	CustomData              interface{} `json:"customData"`
	GiftRegistryData        interface{} `json:"giftRegistryData"`
	HooksData               interface{} `json:"hooksData"`
	IgnoreProfileData       bool        `json:"ignoreProfileData"`
	InvoiceData             interface{} `json:"invoiceData"`
	IsCheckedIn             bool        `json:"isCheckedIn"`
	ItemMetadata ItemMeta `json:"itemMetadata"`
	Items []ItemData `json:"items"`
	ItemsOrdination interface{} `json:"itemsOrdination"`
	LoggedIn        bool        `json:"loggedIn"`
	MarketingData MarketingData `json:"marketingData"`
	Messages      []interface{} `json:"messages"`
	OpenTextField interface{}   `json:"openTextField"`
	OrderFormID   string        `json:"orderFormId"`
	PaymentData PaymentData `json:"paymentData"`
	RatesAndBenefitsData RatesAndBenefitsData `json:"ratesAndBenefitsData"`
	SalesChannel    string        `json:"salesChannel"`
	SelectableGifts []interface{} `json:"selectableGifts"`
	Sellers         []Seller `json:"sellers"`
	ShippingData ShippingData `json:"shippingData"`
	StoreID              interface{} `json:"storeId"`
	StorePreferencesData StorePreferencesData `json:"storePreferencesData"`
	SubscriptionData interface{} `json:"subscriptionData"`
	Totalizers       []Totalizer `json:"totalizers"`
	UserProfileID interface{} `json:"userProfileId"`
	UserType      interface{} `json:"userType"`
	Value         int64       `json:"value"`
}

type VtexRemoveItemResponse struct {
	AllowManualPrice       bool        `json:"allowManualPrice"`
	CanEditData            bool        `json:"canEditData"`
	CheckedInPickupPointID interface{} `json:"checkedInPickupPointId"`
	ClientPreferencesData ClientPreferencesData `json:"clientPreferencesData"`
	ClientProfileData       interface{} `json:"clientProfileData"`
	CommercialConditionData interface{} `json:"commercialConditionData"`
	CustomData              interface{} `json:"customData"`
	GiftRegistryData        interface{} `json:"giftRegistryData"`
	HooksData               interface{} `json:"hooksData"`
	IgnoreProfileData       bool        `json:"ignoreProfileData"`
	InvoiceData             interface{} `json:"invoiceData"`
	IsCheckedIn             bool        `json:"isCheckedIn"`
	ItemMetadata            struct {
		Items []ItemMeta
	} `json:"itemMetadata"`
	Items           []interface{} `json:"items"`
	ItemsOrdination interface{}   `json:"itemsOrdination"`
	LoggedIn        bool          `json:"loggedIn"`
	MarketingData MarketingData `json:"marketingData"`
	Messages      []Message `json:"messages"`
	OpenTextField interface{}   `json:"openTextField"`
	OrderFormID   string        `json:"orderFormId"`
	PaymentData PaymentData `json:"paymentData"`
	RatesAndBenefitsData interface{}   `json:"ratesAndBenefitsData"`
	SalesChannel         string        `json:"salesChannel"`
	SelectableGifts      []interface{} `json:"selectableGifts"`
	Sellers              []interface{} `json:"sellers"`
	ShippingData ShippingData `json:"shippingData"`
	StoreID              interface{} `json:"storeId"`
	StorePreferencesData StorePreferencesData `json:"storePreferencesData"`
	SubscriptionData interface{}   `json:"subscriptionData"`
	Totalizers       []interface{} `json:"totalizers"`
	UserProfileID    interface{}   `json:"userProfileId"`
	UserType         interface{}   `json:"userType"`
	Value            int64         `json:"value"`
}

type VtexZipcodeResponse struct {
	PostalCode     string    `json:"postalCode"`
	City           string    `json:"city"`
	State          string    `json:"state"`
	Country        string    `json:"country"`
	Street         string    `json:"street"`
	Number         string    `json:"number"`
	Neighborhood   string    `json:"neighborhood"`
	Complement     string    `json:"complement"`
	Reference      string    `json:"reference"`
	GeoCoordinates []float64 `json:"geoCoordinates"`
}

type VtexClientProfileDataResponse struct {
	OrderFormID            string        `json:"orderFormId"`
	SalesChannel           string        `json:"salesChannel"`
	LoggedIn               bool          `json:"loggedIn"`
	IsCheckedIn            bool          `json:"isCheckedIn"`
	StoreID                interface{}   `json:"storeId"`
	CheckedInPickupPointID interface{}   `json:"checkedInPickupPointId"`
	AllowManualPrice       bool          `json:"allowManualPrice"`
	CanEditData            bool          `json:"canEditData"`
	UserProfileID          string        `json:"userProfileId"`
	UserType               interface{}   `json:"userType"`
	IgnoreProfileData      bool          `json:"ignoreProfileData"`
	Value                  int           `json:"value"`
	Messages               []interface{} `json:"messages"`
	Items                  []ItemData `json:"items"`
	SelectableGifts []interface{} `json:"selectableGifts"`
	Totalizers      []Totalizer `json:"totalizers"`
	ShippingData ShippingData `json:"shippingData"`
	ClientProfileData ClientProfileData `json:"clientProfileData"`
	PaymentData PaymentData `json:"paymentData"`
	MarketingData MarketingData `json:"marketingData"`
	Sellers []Seller `json:"sellers"`
	ClientPreferencesData ClientPreferencesData `json:"clientPreferencesData"`
	CommercialConditionData interface{} `json:"commercialConditionData"`
	StorePreferencesData StorePreferencesData `json:"storePreferencesData"`
	GiftRegistryData interface{} `json:"giftRegistryData"`
	OpenTextField    interface{} `json:"openTextField"`
	InvoiceData      interface{} `json:"invoiceData"`
	CustomData       interface{} `json:"customData"`
	ItemMetadata     struct {
		Items []ItemMeta `json:"items"`
	} `json:"itemMetadata"`
	HooksData            interface{} `json:"hooksData"`
	RatesAndBenefitsData RatesAndBenefitsData `json:"ratesAndBenefitsData"`
	SubscriptionData interface{} `json:"subscriptionData"`
	ItemsOrdination  interface{} `json:"itemsOrdination"`
}

type VtexTransactionResponse struct {
	AllowManualPrice       bool        `json:"allowManualPrice"`
	CanEditData            bool        `json:"canEditData"`
	CheckedInPickupPointID interface{} `json:"checkedInPickupPointId"`
	ClientPreferencesData ClientPreferencesData `json:"clientPreferencesData"`
	ClientProfileData ClientProfileData `json:"clientProfileData"`
	CommercialConditionData     interface{} `json:"commercialConditionData"`
	CustomData                  interface{} `json:"customData"`
	GatewayCallbackTemplatePath string      `json:"gatewayCallbackTemplatePath"`
	GiftRegistryData            interface{} `json:"giftRegistryData"`
	HooksData                   interface{} `json:"hooksData"`
	ID                          string      `json:"id"`
	IgnoreProfileData           bool        `json:"ignoreProfileData"`
	InvoiceData                 interface{} `json:"invoiceData"`
	IsCheckedIn                 bool        `json:"isCheckedIn"`
	ItemMetadata                struct {
		Items []ItemMeta `json:"items"`
	} `json:"itemMetadata"`
	Items []ItemData `json:"items"`
	ItemsOrdination      interface{} `json:"itemsOrdination"`
	LoggedIn             bool        `json:"loggedIn"`
	MarketingData        interface{} `json:"marketingData"`
	MerchantTransactions []MerchantTransaction `json:"merchantTransactions"`
	Messages      []interface{} `json:"messages"`
	OpenTextField interface{}   `json:"openTextField"`
	OrderFormID   string        `json:"orderFormId"`
	PaymentData   PaymentData `json:"paymentData"`
	RatesAndBenefitsData RatesAndBenefitsData `json:"ratesAndBenefitsData"`
	ReceiverURI     string        `json:"receiverUri"`
	SalesChannel    string        `json:"salesChannel"`
	SelectableGifts []interface{} `json:"selectableGifts"`
	Sellers         []Seller `json:"sellers"`
	ShippingData ShippingData `json:"shippingData"`
	StoreID              interface{} `json:"storeId"`
	StorePreferencesData StorePreferencesData `json:"storePreferencesData"`
	SubscriptionData interface{} `json:"subscriptionData"`
	Totalizers       []Totalizer `json:"totalizers"`
	UserProfileID string      `json:"userProfileId"`
	UserType      interface{} `json:"userType"`
	Value         int64       `json:"value"`
	CookieName string `json:"cookieName"`
	CookieValue string`json:"cookieValue"`
}
