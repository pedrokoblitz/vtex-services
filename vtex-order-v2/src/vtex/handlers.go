package vtex

import (
	// "fmt"
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"io/ioutil"
	// "strconv"
	// "time"
	// "github.com/davecgh/go-spew/spew"
	// "github.com/jinzhu/copier"
	// "strings"
)


func PostOrderForm(w http.ResponseWriter, r *http.Request) {
	var httpResponse *http.Response
	var vtexRequest VtexOrderFormRequest
	var vtexResponse VtexOrderFormResponse
	var payload []byte
	var err error

	payload, err = ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.Unmarshal(payload, &vtexRequest)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	/////////////////////////////

	httpResponse = apiPostOrderForm(payload)
	payload, err = ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	httpResponse.Body.Close()
	log.Println("ORDER FORM", httpResponse.StatusCode)

	if httpResponse.StatusCode != 200 {
		http.Error(w, string(payload), httpResponse.StatusCode)
		return
	}

	err = json.Unmarshal(payload, &vtexResponse)

	/////////////////////////////

	payload, err = json.Marshal(vtexResponse)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}

func PostAddToCart(w http.ResponseWriter, r *http.Request) {
	var httpResponse *http.Response
	var vtexRequest VtexAddItemRequest
	var vtexResponse VtexAddItemResponse
	var payload []byte
	var err error

	params := mux.Vars(r)
	orderFormID := params["orderFormId"]

	payload, err = ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.Unmarshal(payload, &vtexRequest)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	/////////////////////////////

	httpResponse = apiPostAddToCart(payload, orderFormID)
	payload, err = ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	httpResponse.Body.Close()
	log.Println("ADD TO CART", httpResponse.StatusCode)

	if httpResponse.StatusCode != 200 {
		http.Error(w, string(payload), httpResponse.StatusCode)
		return
	}

	err = json.Unmarshal(payload, &vtexResponse)

	/////////////////////////////

	payload, err = json.Marshal(vtexResponse)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}

func PostRemoveFromCart(w http.ResponseWriter, r *http.Request) {
	var httpResponse *http.Response
	var vtexRequest VtexRemoveItemRequest
	var vtexResponse VtexRemoveItemResponse
	var payload []byte
	var err error

	params := mux.Vars(r)
	orderFormID := params["orderFormId"]

	payload, err = ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.Unmarshal(payload, &vtexRequest)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	/////////////////////////////

	httpResponse = apiPostRemoveFromCart(payload, orderFormID)
	payload, err = ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	httpResponse.Body.Close()
	log.Println("REMOVE FROM CART", httpResponse.StatusCode)

	if httpResponse.StatusCode != 200 {
		http.Error(w, string(payload), httpResponse.StatusCode)
		return
	}

	err = json.Unmarshal(payload, &vtexResponse)

	/////////////////////////////

	payload, err = json.Marshal(vtexResponse)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}

func PostAddCoupon(w http.ResponseWriter, r *http.Request) {
	var httpResponse *http.Response
	var vtexRequest VtexCouponRequest
	var vtexResponse VtexCouponResponse
	var payload []byte
	var err error

	params := mux.Vars(r)
	orderFormID := params["orderFormId"]

	payload, err = ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.Unmarshal(payload, &vtexRequest)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}


	/////////////////////////////

	httpResponse = apiPostAddCoupon(payload, orderFormID)
	payload, err = ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	httpResponse.Body.Close()
	log.Println("ADD COUPON", httpResponse.StatusCode)

	if httpResponse.StatusCode != 200 {
		http.Error(w, string(payload), httpResponse.StatusCode)
		return
	}

	err = json.Unmarshal(payload, &vtexResponse)

	/////////////////////////////

	payload, err = json.Marshal(vtexResponse)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}

func GetZipcode(w http.ResponseWriter, r *http.Request) {
	var httpResponse *http.Response
	// var vtexRequest
	var vtexResponse VtexZipcodeResponse
	var payload []byte
	var err error

	params := mux.Vars(r)
	zipcode := params["zipcode"]

	payload, err = ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	/////////////////////////////

	httpResponse = apiGetZipcode(zipcode)
	payload, err = ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	httpResponse.Body.Close()
	log.Println("GET ZIPCODE", httpResponse.StatusCode)

	if httpResponse.StatusCode != 200 {
		http.Error(w, string(payload), httpResponse.StatusCode)
		return
	}

	err = json.Unmarshal(payload, &vtexResponse)

	/////////////////////////////

	payload, err = json.Marshal(vtexResponse)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}

func PostShippingData(w http.ResponseWriter, r *http.Request) {
	var httpResponse *http.Response
	var vtexRequest VtexShippingDataRequest
	var vtexResponse VtexShippingDataResponse
	var payload []byte
	var err error

	params := mux.Vars(r)
	orderFormID := params["orderFormId"]

	payload, err = ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.Unmarshal(payload, &vtexRequest)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	/////////////////////////////

	httpResponse = apiPostShippingData(payload, orderFormID)
	payload, err = ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	httpResponse.Body.Close()
	log.Println("SHIPPING DATA", httpResponse.StatusCode)

	if httpResponse.StatusCode != 200 {
		http.Error(w, string(payload), httpResponse.StatusCode)
		return
	}

	err = json.Unmarshal(payload, &vtexResponse)

	/////////////////////////////

	payload, err = json.Marshal(vtexResponse)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}


func PostClientProfileData(w http.ResponseWriter, r *http.Request) {
	var httpResponse *http.Response
	var vtexRequest VtexClientProfileDataRequest
	var vtexResponse VtexClientProfileDataResponse
	var payload []byte
	var err error

	params := mux.Vars(r)
	orderFormID := params["orderFormId"]

	payload, err = ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.Unmarshal(payload, &vtexRequest)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	/////////////////////////////

	httpResponse = apiPostClientProfileData(payload, orderFormID)
	payload, err = ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	httpResponse.Body.Close()
	log.Println("CLIENT PROFILE DATA", httpResponse.StatusCode)

	if httpResponse.StatusCode != 200 {
		http.Error(w, string(payload), httpResponse.StatusCode)
		return
	}

	err = json.Unmarshal(payload, &vtexResponse)

	/////////////////////////////

	payload, err = json.Marshal(vtexResponse)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}

func PostSimulation(w http.ResponseWriter, r *http.Request) {
	var httpResponse *http.Response
	var vtexRequest VtexSimulationRequest
	var vtexResponse VtexSimulationResponse
	var payload []byte
	var err error

	payload, err = ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.Unmarshal(payload, &vtexRequest)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	/////////////////////////////

	httpResponse = apiPostSimulation(payload)
	payload, err = ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	httpResponse.Body.Close()
	log.Println("SIMULATION", httpResponse.StatusCode)

	if httpResponse.StatusCode != 200 {
		http.Error(w, string(payload), httpResponse.StatusCode)
		return
	}

	err = json.Unmarshal(payload, &vtexResponse)

	/////////////////////////////

	payload, err = json.Marshal(vtexResponse)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}

func PostMarketingData(w http.ResponseWriter, r *http.Request) {
	var httpResponse *http.Response
	var vtexRequest VtexMktRequest
	var vtexResponse VtexMktResponse
	var payload []byte
	var err error

	params := mux.Vars(r)
	orderFormID := params["orderFormId"]

	payload, err = ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.Unmarshal(payload, &vtexRequest)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	/////////////////////////////

	httpResponse = apiPostMarketingData(payload, orderFormID)
	payload, err = ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	httpResponse.Body.Close()
	log.Println("MARKETING DATA", httpResponse.StatusCode)

	if httpResponse.StatusCode != 200 {
		http.Error(w, string(payload), httpResponse.StatusCode)
		return
	}

	err = json.Unmarshal(payload, &vtexResponse)

	/////////////////////////////

	payload, err = json.Marshal(vtexResponse)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}

func PostPaymentData(w http.ResponseWriter, r *http.Request) {
	var httpResponse *http.Response
	var vtexRequest VtexPaymentDataRequest
	var vtexResponse VtexPaymentDataResponse
	var payload []byte
	var err error

	params := mux.Vars(r)
	orderFormID := params["orderFormId"]

	payload, err = ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.Unmarshal(payload, &vtexRequest)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	/////////////////////////////

	httpResponse = apiPostPaymentData(payload, orderFormID)
	payload, err = ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	httpResponse.Body.Close()
	log.Println("PAYMENT DATA", httpResponse.StatusCode)

	if httpResponse.StatusCode != 200 {
		http.Error(w, string(payload), httpResponse.StatusCode)
		return
	}

	err = json.Unmarshal(payload, &vtexResponse)

	/////////////////////////////


	payload, err = json.Marshal(vtexResponse)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}

func PostTransaction(w http.ResponseWriter, r *http.Request) {
	var httpResponse *http.Response
	var vtexRequest VtexTransactionRequest
	var vtexResponse VtexTransactionResponse
	var payload []byte
	var err error

	params := mux.Vars(r)
	orderFormID := params["orderFormId"]

	payload, err = ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.Unmarshal(payload, &vtexRequest)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	/////////////////////////////

	httpResponse = apiPostTransaction(payload, orderFormID)
	payload, err = ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	httpResponse.Body.Close()
	log.Println("TRANSACTION", httpResponse.StatusCode)

	if httpResponse.StatusCode != 200 {
		http.Error(w, string(payload), httpResponse.StatusCode)
		return
	}

	var chkoAuth *http.Cookie
	for _, cookie := range httpResponse.Cookies() {
		if cookie.Name == "Vtex_CHKO_Auth" {
			chkoAuth = cookie
		}
	}

	err = json.Unmarshal(payload, &vtexResponse)
	vtexResponse.CookieName = chkoAuth.Name
	vtexResponse.CookieValue = chkoAuth.Value

	payload, err = json.Marshal(vtexResponse)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	/////////////////////////////

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}

func PostPaymentConfirmation(w http.ResponseWriter, r *http.Request) {
	var httpResponse *http.Response
	var vtexRequest VtexPaymentInfoRequest
	var paymentInfo PaymentInfo
	// var vtexResponse VtexTransactionResponse
	var payload []byte
	var err error

	payload, err = ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
    
	err = json.Unmarshal(payload, &vtexRequest)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

    cookie := http.Cookie{
        Name:    "Vtex_CHKO_Auth",
        Value:   vtexRequest.CookieValue,
    }

	paymentInfo.PaymentSystem = vtexRequest.PaymentSystem
	paymentInfo.PaymentSystemName = vtexRequest.PaymentSystemName
	paymentInfo.Group = vtexRequest.Group
	paymentInfo.Installments = vtexRequest.Installments
	paymentInfo.InstallmentsInterestRate = vtexRequest.InstallmentsInterestRate
	paymentInfo.InstallmentsValue = vtexRequest.InstallmentsValue
	paymentInfo.Value = vtexRequest.Value
	paymentInfo.ReferenceValue = vtexRequest.ReferenceValue
	paymentInfo.Fields = vtexRequest.Fields
	paymentInfo.Transaction = vtexRequest.Transaction
	paymentInfo.CurrencyCode = vtexRequest.CurrencyCode

	payload, err = json.Marshal(paymentInfo)

	httpResponse = apiPostPayment(paymentInfo.Transaction.ID, payload)
	payload, err = ioutil.ReadAll(httpResponse.Body)

	log.Println("PAYMENT", httpResponse.StatusCode)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	httpResponse.Body.Close()

	httpResponse = apiPostProcessPayment(vtexRequest.OrderGroup, &cookie)
	payload, err = ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	httpResponse.Body.Close()
	log.Println("PAYMENT PROCESSING", httpResponse.StatusCode)

}


func GetProfile(w http.ResponseWriter, r *http.Request) {
	var res *http.Response
	var payload []byte
	var err error

	params := mux.Vars(r)
	email := params["email"]

	res = apiGetProfile(email)
	payload, err = ioutil.ReadAll(res.Body)
	if err != nil {
		// log.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}

	res.Body.Close()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}


func PostCartSimulation(w http.ResponseWriter, r *http.Request) {
	var payload []byte
	var res *http.Response
	var err error

	payload, err = ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	res = apiPostCartSimulation(payload)
	log.Println("CART SIMULATION", res.StatusCode)

	payload, err = ioutil.ReadAll(res.Body)
	if err != nil {
		// log.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}
	res.Body.Close()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}
