package vtex

type ReqShippingData struct {
	LogisticsInfo []LogisticsInfo `json:"logisticsInfo"`
	SelectedAddresses []Address `json:"selectedAddresses"`
}

type ReqCcField struct {
	HolderName     string `json:"holderName"`
	CardNumber     string `json:"cardNumber"`
	ValidationCode string `json:"validationCode"`
	DueDate        string `json:"dueDate"`
	AddressID      string `json:"addressId"`
}

type ReqTransaction struct {
	ID           string `json:"id"`
	MerchantName string `json:"merchantName"`
}

type ReqLogisticsInfo struct {
	AddressID               string `json:"addressId"`
	ItemIndex               int    `json:"itemIndex"`
	SelectedDeliveryChannel string `json:"selectedDeliveryChannel"`
	SelectedSLA             string `json:"selectedSla"`
	Price       int    `json:"price"`
} 

type ReqAddress struct {
	AddressType    string      `json:"addressType"`
	ReceiverName   string      `json:"receiverName"`
	AddressID      string      `json:"addressId"`
	PostalCode     string      `json:"postalCode"`
	City           string      `json:"city"`
	State          string      `json:"state"`
	Country        string      `json:"country"`
	Street         string      `json:"street"`
	Number         string      `json:"number"`
	Neighborhood   string      `json:"neighborhood"`
	Complement     string      `json:"complement"`
	Reference      string      `json:"reference"`
	GeoCoordinates []float64   `json:"geoCoordinates"`
	AddressQuery   string      `json:"addressQuery"`
}

type ReqPayment struct {
	PaymentSystem  string `json:"paymentSystem"`
	ReferenceValue int    `json:"referenceValue"`
	Value          int    `json:"value"`
	Installments   int    `json:"installments"`
}

type ReqOrderItemToAdd struct {
	ID       string `json:"id"`
	Quantity int64  `json:"quantity"`
	Seller   string `json:"seller"`
}

type ReqOrderItemToRemove struct {
	Index    string `json:"index"`
	Quantity int64  `json:"quantity"`
}

type VtexPaymentInfoRequest struct {
	PaymentSystem            string    `json:"paymentSystem"`
	PaymentSystemName        string `json:"paymentSystemName"`
	Group                    string `json:"group"`
	Installments             int    `json:"installments"`
	InstallmentsInterestRate int    `json:"installmentsInterestRate"`
	InstallmentsValue        int    `json:"installmentsValue"`
	Value                    int    `json:"value"`
	ReferenceValue           int    `json:"referenceValue"`
	Fields                   ReqCcField `json:"fields"`
	Transaction ReqTransaction `json:"transaction"`
	CurrencyCode string `json:"currencyCode"`
	CookieValue string `json:"cookieValue"`
	OrderGroup string `json:"OrderGroup"`
}

type PaymentInfo struct {
	PaymentSystem            string    `json:"paymentSystem"`
	PaymentSystemName        string `json:"paymentSystemName"`
	Group                    string `json:"group"`
	Installments             int    `json:"installments"`
	InstallmentsInterestRate int    `json:"installmentsInterestRate"`
	InstallmentsValue        int    `json:"installmentsValue"`
	Value                    int    `json:"value"`
	ReferenceValue           int    `json:"referenceValue"`
	Fields                   ReqCcField `json:"fields"`
	Transaction ReqTransaction `json:"transaction"`
	CurrencyCode string `json:"currencyCode"`
}

type VtexShippingDataRequest struct {
	LogisticsInfo []ReqLogisticsInfo `json:"logisticsInfo"`
	SelectedAddresses []ReqAddress `json:"selectedAddresses"`
	ClearAddressIfPostalCodeNotFound bool     `json:"clearAddressIfPostalCodeNotFound"`
	ExpectedOrderFormSections        []string `json:"expectedOrderFormSections"`
}

type VtexPaymentDataRequest struct {
	ExpectedOrderFormSections []string      `json:"expectedOrderFormSections"`
	GiftCards                 []interface{} `json:"giftCards"`
	Payments                  []ReqPayment `json:"payments"`
}

type VtexTransactionRequest struct {
	ExpectedOrderFormSections []string `json:"expectedOrderFormSections"`
	InterestValue             int64    `json:"interestValue"`
	OptinNewsLetter           bool     `json:"optinNewsLetter"`
	ReferenceID               string   `json:"referenceId"`
	ReferenceValue            int64    `json:"referenceValue"`
	SavePersonalData          bool     `json:"savePersonalData"`
	Value                     int64    `json:"value"`
}

type VtexSimulationRequest struct {
	OrderFormID  string `json:"orderFormId"`
	ShippingData ReqShippingData `json:"shippingData"`
}

type VtexOrderFormRequest struct {
	ExpectedOrderFormSections []string `json:"expectedOrderFormSections"`
}

type VtexAddItemRequest struct {
	ExpectedOrderFormSections []string `json:"expectedOrderFormSections"`
	OrderItems                []ReqOrderItemToAdd `json:"orderItems"`
}

type VtexCouponRequest struct {
	ExpectedOrderFormSections []string `json:"expectedOrderFormSections"`
	Text                      string   `json:"text"`
}

type VtexMktRequest struct {
	ExpectedOrderFormSections []string `json:"expectedOrderFormSections"`
	UtmCampaign               string   `json:"utmCampaign"`
	UtmMedium                 string   `json:"utmMedium"`
	UtmSource                 string   `json:"utmSource"`
	UtmiCampaign              string   `json:"utmiCampaign"`
	UtmiPart                  string   `json:"utmiPart"`
}

type VtexRemoveItemRequest struct {
	ExpectedOrderFormSections []string `json:"expectedOrderFormSections"`
	OrderItems                []ReqOrderItemToRemove `json:"orderItems"`
}

type VtexClientProfileDataRequest struct {
	FirstEmail                string   `json:"firstEmail"`
	Email                     string   `json:"email"`
	FirstName                 string   `json:"firstName"`
	LastName                  string   `json:"lastName"`
	Document                  string   `json:"document"`
	Phone                     string   `json:"phone"`
	DocumentType              string   `json:"documentType"`
	IsCorporate               bool     `json:"isCorporate"`
	StateInscription          string   `json:"stateInscription"`
	ExpectedOrderFormSections []string `json:"expectedOrderFormSections"`
}
