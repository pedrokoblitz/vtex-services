package vtex
/*
import (
	"net/http"
	"time"
	"strings"
	"log"
)

func apiPutOrder(payload []byte) *http.Response {
	var req *http.Request
	var err error
	var url string

	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url = "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orders"
	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("PUT", url, reqPayload)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}
	return res
}


func apiGetOrder(orderID string) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/oms/pvt/orders/" + orderID
	req, err = http.NewRequest("GET", url, nil)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

type Order struct {
	Items             []Item            `json:"items"`
	ClientProfileData ClientProfileData `json:"clientProfileData"`
	ShippingData      ShippingData      `json:"shippingData"`
	PaymentData       PaymentData       `json:"paymentData"`
}


type ApiOrderRequest struct {
	Order FinalOrder `json:"order"`
	PaymentInfo []PaymentInfo `json:"payments"`
}

type ApiCartSimulationRequest struct {
	Items []Item `json:"items"`
	PostalCode string `json:postalCode`
	Country string `json:country`
}

type Item struct {
	ID          string        `json:"id"`
	Quantity    int           `json:"quantity"`
	Seller      string        `json:"seller"`
	Price       int           `json:"price"`
	RewardValue int           `json:"rewardValue"`
	Offerings   []interface{} `json:"offerings"`
	PriceTags   []interface{} `json:"priceTags"`
	IsGift      bool          `json:"isGift"`
}

type PaymentData struct {
	ID       string    `json:"id"`
	Payments []Payment `json:"payments"`
}

type FinalOrder struct {
	Items             []Item            `json:"items"`
	ClientProfileData ClientProfileData `json:"clientProfileData"`
	ShippingData      ShippingData      `json:"shippingData"`
	PaymentData       PaymentData       `json:"paymentData"`
	MarketingData     MarketingData     `json:"marketingData"`
}

type MarketingData struct {
	Coupon        interface{}   `json:"coupon"`
	MarketingTags []interface{} `json:"marketingTags"`
	UtmCampaign   interface{}   `json:"utmCampaign"`
	UtmMedium     interface{}   `json:"utmMedium"`
	UtmSource     interface{}   `json:"utmSource"`
	UtmiCampaign  interface{}   `json:"utmiCampaign"`
	UtmiPart      interface{}   `json:"utmiPart"`
	Utmipage      interface{}   `json:"utmipage"`
}

type OrderResponse struct {
	OrderForm       interface{} `json:"orderForm"`
	TransactionData TransactionData `json:"transactionData"`
	Orders []struct {
		OrderID                string      `json:"orderId"`
		OrderGroup             string      `json:"orderGroup"`
		State                  string      `json:"state"`
		IsCheckedIn            bool        `json:"isCheckedIn"`
		SellerOrderID          string      `json:"sellerOrderId"`
		StoreID                interface{} `json:"storeId"`
		CheckedInPickupPointID interface{} `json:"checkedInPickupPointId"`
		Value                  int         `json:"value"`
		Items                  []struct {
			UniqueID              string        `json:"uniqueId"`
			ID                    string        `json:"id"`
			ProductID             string        `json:"productId"`
			RefID                 string        `json:"refId"`
			Ean                   string        `json:"ean"`
			Name                  string        `json:"name"`
			SkuName               string        `json:"skuName"`
			ModalType             interface{}   `json:"modalType"`
			ParentItemIndex       interface{}   `json:"parentItemIndex"`
			ParentAssemblyBinding interface{}   `json:"parentAssemblyBinding"`
			Assemblies            []interface{} `json:"assemblies"`
			PriceValidUntil       time.Time     `json:"priceValidUntil"`
			Tax                   int           `json:"tax"`
			Price                 int           `json:"price"`
			ListPrice             int           `json:"listPrice"`
			ManualPrice           interface{}   `json:"manualPrice"`
			SellingPrice          int           `json:"sellingPrice"`
			RewardValue           int           `json:"rewardValue"`
			IsGift                bool          `json:"isGift"`
			AdditionalInfo        struct {
				BrandName      string      `json:"brandName"`
				BrandID        string      `json:"brandId"`
				OfferingInfo   interface{} `json:"offeringInfo"`
				OfferingType   interface{} `json:"offeringType"`
				OfferingTypeID interface{} `json:"offeringTypeId"`
			} `json:"additionalInfo"`
			PreSaleDate        interface{} `json:"preSaleDate"`
			ProductCategoryIds string      `json:"productCategoryIds"`
			ProductCategories  struct {
				Num13 string `json:"13"`
			} `json:"productCategories"`
			Quantity            int           `json:"quantity"`
			Seller              string        `json:"seller"`
			SellerChain         []string      `json:"sellerChain"`
			ImageURL            string        `json:"imageUrl"`
			DetailURL           string        `json:"detailUrl"`
			Components          []interface{} `json:"components"`
			BundleItems         []interface{} `json:"bundleItems"`
			Attachments         []interface{} `json:"attachments"`
			AttachmentOfferings []interface{} `json:"attachmentOfferings"`
			Offerings           []interface{} `json:"offerings"`
			PriceTags           []interface{} `json:"priceTags"`
			Availability        string        `json:"availability"`
			MeasurementUnit     string        `json:"measurementUnit"`
			UnitMultiplier      float64           `json:"unitMultiplier"`
		} `json:"items"`
		Sellers []struct {
			ID   string `json:"id"`
			Name string `json:"name"`
			Logo string `json:"logo"`
		} `json:"sellers"`
		Totals []struct {
			ID    string `json:"id"`
			Name  string `json:"name"`
			Value int    `json:"value"`
		} `json:"totals"`
		ClientProfileData struct {
			Email                    string      `json:"email"`
			FirstName                string      `json:"firstName"`
			LastName                 string      `json:"lastName"`
			Document                 string      `json:"document"`
			DocumentType             string      `json:"documentType"`
			Phone                    string      `json:"phone"`
			CorporateName            interface{} `json:"corporateName"`
			TradeName                interface{} `json:"tradeName"`
			CorporateDocument        interface{} `json:"corporateDocument"`
			StateInscription         interface{} `json:"stateInscription"`
			CorporatePhone           interface{} `json:"corporatePhone"`
			IsCorporate              bool        `json:"isCorporate"`
			ProfileCompleteOnLoading interface{} `json:"profileCompleteOnLoading"`
			ProfileErrorOnLoading    interface{} `json:"profileErrorOnLoading"`
			CustomerClass            interface{} `json:"customerClass"`
		} `json:"clientProfileData"`
		RatesAndBenefitsData struct {
			RateAndBenefitsIdentifiers []interface{} `json:"rateAndBenefitsIdentifiers"`
			Teaser                     []interface{} `json:"teaser"`
		} `json:"ratesAndBenefitsData"`
		ShippingData struct {
			Address struct {
				AddressType    string      `json:"addressType"`
				ReceiverName   string      `json:"receiverName"`
				AddressID      string      `json:"addressId"`
				PostalCode     string      `json:"postalCode"`
				City           string      `json:"city"`
				State          string      `json:"state"`
				Country        string      `json:"country"`
				Street         string      `json:"street"`
				Number         string      `json:"number"`
				Neighborhood   string      `json:"neighborhood"`
				Complement     string      `json:"complement"`
				Reference      interface{} `json:"reference"`
				GeoCoordinates []float64   `json:"geoCoordinates"`
			} `json:"address"`
			LogisticsInfo []struct {
				ItemIndex               int    `json:"itemIndex"`
				SelectedSLA             string `json:"selectedSla"`
				SelectedDeliveryChannel string `json:"selectedDeliveryChannel"`
				AddressID               string `json:"addressId"`
				Slas                    []struct {
					ID              string `json:"id"`
					DeliveryChannel string `json:"deliveryChannel"`
					Name            string `json:"name"`
					DeliveryIds     []struct {
						CourierID   string `json:"courierId"`
						WarehouseID string `json:"warehouseId"`
						DockID      string `json:"dockId"`
						CourierName string `json:"courierName"`
						Quantity    int    `json:"quantity"`
					} `json:"deliveryIds"`
					ShippingEstimate         string        `json:"shippingEstimate"`
					ShippingEstimateDate     interface{}   `json:"shippingEstimateDate"`
					LockTTL                  string        `json:"lockTTL"`
					AvailableDeliveryWindows []interface{} `json:"availableDeliveryWindows"`
					DeliveryWindow           interface{}   `json:"deliveryWindow"`
					Price                    int           `json:"price"`
					ListPrice                int           `json:"listPrice"`
					Tax                      int           `json:"tax"`
					PickupStoreInfo          struct {
						IsPickupStore  bool        `json:"isPickupStore"`
						FriendlyName   interface{} `json:"friendlyName"`
						Address        interface{} `json:"address"`
						AdditionalInfo interface{} `json:"additionalInfo"`
						DockID         interface{} `json:"dockId"`
					} `json:"pickupStoreInfo"`
					PickupPointID  interface{} `json:"pickupPointId"`
					PickupDistance float64         `json:"pickupDistance"`
					PolygonName    interface{} `json:"polygonName"`
				} `json:"slas"`
				ShipsTo          []string `json:"shipsTo"`
				ItemID           string   `json:"itemId"`
				DeliveryChannels []struct {
					ID string `json:"id"`
				} `json:"deliveryChannels"`
			} `json:"logisticsInfo"`
			SelectedAddresses []struct {
				AddressType    string      `json:"addressType"`
				ReceiverName   string      `json:"receiverName"`
				AddressID      string      `json:"addressId"`
				PostalCode     string      `json:"postalCode"`
				City           string      `json:"city"`
				State          string      `json:"state"`
				Country        string      `json:"country"`
				Street         string      `json:"street"`
				Number         string      `json:"number"`
				Neighborhood   string      `json:"neighborhood"`
				Complement     string      `json:"complement"`
				Reference      interface{} `json:"reference"`
				GeoCoordinates []float64   `json:"geoCoordinates"`
			} `json:"selectedAddresses"`
			AvailableAddresses []struct {
				AddressType    string      `json:"addressType"`
				ReceiverName   string      `json:"receiverName"`
				AddressID      string      `json:"addressId"`
				PostalCode     string      `json:"postalCode"`
				City           string      `json:"city"`
				State          string      `json:"state"`
				Country        string      `json:"country"`
				Street         string      `json:"street"`
				Number         string      `json:"number"`
				Neighborhood   string      `json:"neighborhood"`
				Complement     string      `json:"complement"`
				Reference      interface{} `json:"reference"`
				GeoCoordinates []float64   `json:"geoCoordinates"`
			} `json:"availableAddresses"`
			PickupPoints []struct {
				FriendlyName string `json:"friendlyName"`
				Address      struct {
					AddressType    string      `json:"addressType"`
					ReceiverName   interface{} `json:"receiverName"`
					AddressID      string      `json:"addressId"`
					PostalCode     string      `json:"postalCode"`
					City           string      `json:"city"`
					State          string      `json:"state"`
					Country        string      `json:"country"`
					Street         string      `json:"street"`
					Number         string      `json:"number"`
					Neighborhood   string      `json:"neighborhood"`
					Complement     string      `json:"complement"`
					Reference      interface{} `json:"reference"`
					GeoCoordinates []float64   `json:"geoCoordinates"`
				} `json:"address"`
				AdditionalInfo string `json:"additionalInfo"`
				ID             string `json:"id"`
				BusinessHours  []struct {
					DayOfWeek   int    `json:"DayOfWeek"`
					OpeningTime string `json:"OpeningTime"`
					ClosingTime string `json:"ClosingTime"`
				} `json:"businessHours"`
			} `json:"pickupPoints"`
		} `json:"shippingData"`
		PaymentData struct {
			GiftCards    []interface{} `json:"giftCards"`
			Transactions []struct {
				IsActive          bool          `json:"isActive"`
				TransactionID     string        `json:"transactionId"`
				MerchantName      string        `json:"merchantName"`
				Payments          []interface{} `json:"payments"`
				SharedTransaction bool          `json:"sharedTransaction"`
			} `json:"transactions"`
		} `json:"paymentData"`
		ClientPreferencesData   interface{} `json:"clientPreferencesData"`
		CommercialConditionData interface{} `json:"commercialConditionData"`
		GiftRegistryData        interface{} `json:"giftRegistryData"`
		MarketingData           interface{} `json:"marketingData"`
		StorePreferencesData    struct {
			CountryCode        string `json:"countryCode"`
			SaveUserData       bool   `json:"saveUserData"`
			TimeZone           string `json:"timeZone"`
			CurrencyCode       string `json:"currencyCode"`
			CurrencyLocale     int    `json:"currencyLocale"`
			CurrencySymbol     string `json:"currencySymbol"`
			CurrencyFormatInfo struct {
				CurrencyDecimalDigits    int    `json:"currencyDecimalDigits"`
				CurrencyDecimalSeparator string `json:"currencyDecimalSeparator"`
				CurrencyGroupSeparator   string `json:"currencyGroupSeparator"`
				CurrencyGroupSize        int    `json:"currencyGroupSize"`
				StartsWithCurrencySymbol bool   `json:"startsWithCurrencySymbol"`
			} `json:"currencyFormatInfo"`
		} `json:"storePreferencesData"`
		OpenTextField interface{} `json:"openTextField"`
		InvoiceData   interface{} `json:"invoiceData"`
		ItemMetadata  struct {
			Items []struct {
				ID              string        `json:"id"`
				Seller          string        `json:"seller"`
				Name            string        `json:"name"`
				SkuName         string        `json:"skuName"`
				ProductID       string        `json:"productId"`
				RefID           string        `json:"refId"`
				Ean             string        `json:"ean"`
				ImageURL        string        `json:"imageUrl"`
				DetailURL       string        `json:"detailUrl"`
				AssemblyOptions []interface{} `json:"assemblyOptions"`
			} `json:"items"`
		} `json:"itemMetadata"`
		TaxData              interface{} `json:"taxData"`
		CustomData           interface{} `json:"customData"`
		HooksData            interface{} `json:"hooksData"`
		ChangeData           interface{} `json:"changeData"`
		SubscriptionData     interface{} `json:"subscriptionData"`
		SalesChannel         string      `json:"salesChannel"`
		FollowUpEmail        string      `json:"followUpEmail"`
		CreationDate         time.Time   `json:"creationDate"`
		LastChange           time.Time   `json:"lastChange"`
		TimeZoneCreationDate string      `json:"timeZoneCreationDate"`
		TimeZoneLastChange   string      `json:"timeZoneLastChange"`
		IsCompleted          bool        `json:"isCompleted"`
		HostName             string      `json:"hostName"`
		MerchantName         interface{} `json:"merchantName"`
		UserType             string      `json:"userType"`
		RoundingError        int         `json:"roundingError"`
		AllowEdition         bool        `json:"allowEdition"`
		AllowCancellation    bool        `json:"allowCancellation"`
		IsUserDataVisible    bool        `json:"isUserDataVisible"`
		AllowChangeSeller    bool        `json:"allowChangeSeller"`
	} `json:"orders"`
}

type FinalReturnOrder struct {
	OrderID                     string      `json:"orderId"`
	Sequence                    string      `json:"sequence"`
	MarketplaceOrderID          string      `json:"marketplaceOrderId"`
	MarketplaceServicesEndpoint string      `json:"marketplaceServicesEndpoint"`
	SellerOrderID               string      `json:"sellerOrderId"`
	Origin                      string      `json:"origin"`
	AffiliateID                 string      `json:"affiliateId"`
	SalesChannel                string      `json:"salesChannel"`
	MerchantName                interface{} `json:"merchantName"`
	Status                      string      `json:"status"`
	StatusDescription           string      `json:"statusDescription"`
	Value                       int         `json:"value"`
	CreationDate                time.Time   `json:"creationDate"`
	LastChange                  time.Time   `json:"lastChange"`
	OrderGroup                  string      `json:"orderGroup"`
	Totals                      []struct {
		ID    string `json:"id"`
		Name  string `json:"name"`
		Value int    `json:"value"`
	} `json:"totals"`
	Items []struct {
		UniqueID       string `json:"uniqueId"`
		ID             string `json:"id"`
		ProductID      string `json:"productId"`
		Ean            string `json:"ean"`
		LockID         string `json:"lockId"`
		ItemAttachment struct {
			Content struct {
			} `json:"content"`
			Name interface{} `json:"name"`
		} `json:"itemAttachment"`
		Attachments     []interface{} `json:"attachments"`
		Quantity        int           `json:"quantity"`
		Seller          string        `json:"seller"`
		Name            string        `json:"name"`
		RefID           string        `json:"refId"`
		Price           int           `json:"price"`
		ListPrice       int           `json:"listPrice"`
		ManualPrice     interface{}   `json:"manualPrice"`
		PriceTags       []interface{} `json:"priceTags"`
		ImageURL        string        `json:"imageUrl"`
		DetailURL       string        `json:"detailUrl"`
		Components      []interface{} `json:"components"`
		BundleItems     []interface{} `json:"bundleItems"`
		Params          []interface{} `json:"params"`
		Offerings       []interface{} `json:"offerings"`
		SellerSku       string        `json:"sellerSku"`
		PriceValidUntil interface{}   `json:"priceValidUntil"`
		Commission      int           `json:"commission"`
		Tax             int           `json:"tax"`
		PreSaleDate     interface{}   `json:"preSaleDate"`
		AdditionalInfo  struct {
			BrandName             string `json:"brandName"`
			BrandID               string `json:"brandId"`
			CategoriesIds         string `json:"categoriesIds"`
			ProductClusterID      string `json:"productClusterId"`
			CommercialConditionID string `json:"commercialConditionId"`
			Dimension             struct {
				Cubicweight float64 `json:"cubicweight"`
				Height      float64 `json:"height"`
				Length      float64 `json:"length"`
				Weight      float64 `json:"weight"`
				Width       float64 `json:"width"`
			} `json:"dimension"`
			OfferingInfo   interface{} `json:"offeringInfo"`
			OfferingType   interface{} `json:"offeringType"`
			OfferingTypeID interface{} `json:"offeringTypeId"`
		} `json:"additionalInfo"`
		MeasurementUnit       string      `json:"measurementUnit"`
		UnitMultiplier        float64     `json:"unitMultiplier"`
		SellingPrice          int         `json:"sellingPrice"`
		IsGift                bool        `json:"isGift"`
		ShippingPrice         interface{} `json:"shippingPrice"`
		RewardValue           int         `json:"rewardValue"`
		FreightCommission     int         `json:"freightCommission"`
		PriceDefinitions      interface{} `json:"priceDefinitions"`
		TaxCode               interface{} `json:"taxCode"`
		ParentItemIndex       interface{} `json:"parentItemIndex"`
		ParentAssemblyBinding interface{} `json:"parentAssemblyBinding"`
		CallCenterOperator    interface{} `json:"callCenterOperator"`
		SerialNumbers         interface{} `json:"serialNumbers"`
	} `json:"items"`
	MarketplaceItems  []interface{} `json:"marketplaceItems"`
	ClientProfileData struct {
		ID                string      `json:"id"`
		Email             string      `json:"email"`
		FirstName         string      `json:"firstName"`
		LastName          string      `json:"lastName"`
		DocumentType      string      `json:"documentType"`
		Document          string      `json:"document"`
		Phone             string      `json:"phone"`
		CorporateName     interface{} `json:"corporateName"`
		TradeName         interface{} `json:"tradeName"`
		CorporateDocument interface{} `json:"corporateDocument"`
		StateInscription  interface{} `json:"stateInscription"`
		CorporatePhone    interface{} `json:"corporatePhone"`
		IsCorporate       bool        `json:"isCorporate"`
		UserProfileID     string      `json:"userProfileId"`
		CustomerClass     interface{} `json:"customerClass"`
	} `json:"clientProfileData"`
	GiftRegistryData     interface{} `json:"giftRegistryData"`
	MarketingData        interface{} `json:"marketingData"`
	RatesAndBenefitsData struct {
		ID                         string        `json:"id"`
		RateAndBenefitsIdentifiers []interface{} `json:"rateAndBenefitsIdentifiers"`
	} `json:"ratesAndBenefitsData"`
	ShippingData struct {
		ID      string `json:"id"`
		Address struct {
			AddressType    string      `json:"addressType"`
			ReceiverName   string      `json:"receiverName"`
			AddressID      string      `json:"addressId"`
			PostalCode     string      `json:"postalCode"`
			City           string      `json:"city"`
			State          string      `json:"state"`
			Country        string      `json:"country"`
			Street         string      `json:"street"`
			Number         string      `json:"number"`
			Neighborhood   string      `json:"neighborhood"`
			Complement     string      `json:"complement"`
			Reference      interface{} `json:"reference"`
			GeoCoordinates []float64   `json:"geoCoordinates"`
		} `json:"address"`
		LogisticsInfo []struct {
			ItemIndex            int         `json:"itemIndex"`
			SelectedSLA          string      `json:"selectedSla"`
			LockTTL              string      `json:"lockTTL"`
			Price                int         `json:"price"`
			ListPrice            int         `json:"listPrice"`
			SellingPrice         int         `json:"sellingPrice"`
			DeliveryWindow       interface{} `json:"deliveryWindow"`
			DeliveryCompany      string      `json:"deliveryCompany"`
			ShippingEstimate     string      `json:"shippingEstimate"`
			ShippingEstimateDate interface{} `json:"shippingEstimateDate"`
			Slas                 []struct {
				ID               string      `json:"id"`
				Name             string      `json:"name"`
				ShippingEstimate string      `json:"shippingEstimate"`
				DeliveryWindow   interface{} `json:"deliveryWindow"`
				Price            int         `json:"price"`
				DeliveryChannel  string      `json:"deliveryChannel"`
				PickupStoreInfo  struct {
					AdditionalInfo interface{} `json:"additionalInfo"`
					Address        interface{} `json:"address"`
					DockID         interface{} `json:"dockId"`
					FriendlyName   interface{} `json:"friendlyName"`
					IsPickupStore  bool        `json:"isPickupStore"`
				} `json:"pickupStoreInfo"`
				PolygonName interface{} `json:"polygonName"`
				LockTTL     string      `json:"lockTTL"`
			} `json:"slas"`
			ShipsTo     []string `json:"shipsTo"`
			DeliveryIds []struct {
				CourierID   string `json:"courierId"`
				CourierName string `json:"courierName"`
				DockID      string `json:"dockId"`
				Quantity    int    `json:"quantity"`
				WarehouseID string `json:"warehouseId"`
			} `json:"deliveryIds"`
			DeliveryChannel string `json:"deliveryChannel"`
			PickupStoreInfo struct {
				AdditionalInfo interface{} `json:"additionalInfo"`
				Address        interface{} `json:"address"`
				DockID         interface{} `json:"dockId"`
				FriendlyName   interface{} `json:"friendlyName"`
				IsPickupStore  bool        `json:"isPickupStore"`
			} `json:"pickupStoreInfo"`
			AddressID   string      `json:"addressId"`
			PolygonName interface{} `json:"polygonName"`
		} `json:"logisticsInfo"`
		TrackingHints     interface{} `json:"trackingHints"`
		SelectedAddresses []struct {
			AddressID      string      `json:"addressId"`
			AddressType    string      `json:"addressType"`
			ReceiverName   string      `json:"receiverName"`
			Street         string      `json:"street"`
			Number         string      `json:"number"`
			Complement     string      `json:"complement"`
			Neighborhood   string      `json:"neighborhood"`
			PostalCode     string      `json:"postalCode"`
			City           string      `json:"city"`
			State          string      `json:"state"`
			Country        string      `json:"country"`
			Reference      interface{} `json:"reference"`
			GeoCoordinates []float64   `json:"geoCoordinates"`
		} `json:"selectedAddresses"`
	} `json:"shippingData"`
	PaymentData struct {
		Transactions []struct {
			IsActive      bool   `json:"isActive"`
			TransactionID string `json:"transactionId"`
			MerchantName  string `json:"merchantName"`
			Payments      []struct {
				ID                 string      `json:"id"`
				PaymentSystem      string      `json:"paymentSystem"`
				PaymentSystemName  string      `json:"paymentSystemName"`
				Value              int         `json:"value"`
				Installments       int         `json:"installments"`
				ReferenceValue     int         `json:"referenceValue"`
				CardHolder         interface{} `json:"cardHolder"`
				CardNumber         interface{} `json:"cardNumber"`
				FirstDigits        interface{} `json:"firstDigits"`
				LastDigits         interface{} `json:"lastDigits"`
				Cvv2               interface{} `json:"cvv2"`
				ExpireMonth        interface{} `json:"expireMonth"`
				ExpireYear         interface{} `json:"expireYear"`
				URL                string      `json:"url"`
				GiftCardID         interface{} `json:"giftCardId"`
				GiftCardName       interface{} `json:"giftCardName"`
				GiftCardCaption    interface{} `json:"giftCardCaption"`
				RedemptionCode     interface{} `json:"redemptionCode"`
				Group              string      `json:"group"`
				Tid                interface{} `json:"tid"`
				DueDate            interface{} `json:"dueDate"`
				ConnectorResponses struct {
				} `json:"connectorResponses"`
			} `json:"payments"`
		} `json:"transactions"`
	} `json:"paymentData"`
	PackageAttachment struct {
		Packages []interface{} `json:"packages"`
	} `json:"packageAttachment"`
	Sellers []struct {
		ID   string `json:"id"`
		Name string `json:"name"`
		Logo string `json:"logo"`
	} `json:"sellers"`
	CallCenterOperatorData  interface{} `json:"callCenterOperatorData"`
	FollowUpEmail           string      `json:"followUpEmail"`
	LastMessage             interface{} `json:"lastMessage"`
	Hostname                string      `json:"hostname"`
	InvoiceData             interface{} `json:"invoiceData"`
	ChangesAttachment       interface{} `json:"changesAttachment"`
	OpenTextField           interface{} `json:"openTextField"`
	RoundingError           int         `json:"roundingError"`
	OrderFormID             string      `json:"orderFormId"`
	CommercialConditionData interface{} `json:"commercialConditionData"`
	IsCompleted             bool        `json:"isCompleted"`
	CustomData              interface{} `json:"customData"`
	StorePreferencesData    struct {
		CountryCode        string `json:"countryCode"`
		CurrencyCode       string `json:"currencyCode"`
		CurrencyFormatInfo struct {
			CurrencyDecimalDigits    int    `json:"CurrencyDecimalDigits"`
			CurrencyDecimalSeparator string `json:"CurrencyDecimalSeparator"`
			CurrencyGroupSeparator   string `json:"CurrencyGroupSeparator"`
			CurrencyGroupSize        int    `json:"CurrencyGroupSize"`
			StartsWithCurrencySymbol bool   `json:"StartsWithCurrencySymbol"`
		} `json:"currencyFormatInfo"`
		CurrencyLocale int    `json:"currencyLocale"`
		CurrencySymbol string `json:"currencySymbol"`
		TimeZone       string `json:"timeZone"`
	} `json:"storePreferencesData"`
	AllowCancellation bool `json:"allowCancellation"`
	AllowEdition      bool `json:"allowEdition"`
	IsCheckedIn       bool `json:"isCheckedIn"`
	Marketplace       struct {
		BaseURL     string      `json:"baseURL"`
		IsCertified interface{} `json:"isCertified"`
		Name        string      `json:"name"`
	} `json:"marketplace"`
	AuthorizedDate interface{} `json:"authorizedDate"`
	InvoicedDate   interface{} `json:"invoicedDate"`
	CancelReason   interface{} `json:"cancelReason"`
	ItemMetadata   struct {
		Items []struct {
			ID              string        `json:"Id"`
			Seller          string        `json:"Seller"`
			Name            string        `json:"Name"`
			SkuName         string        `json:"SkuName"`
			ProductID       string        `json:"ProductId"`
			RefID           string        `json:"RefId"`
			Ean             string        `json:"Ean"`
			ImageURL        string        `json:"ImageUrl"`
			DetailURL       string        `json:"DetailUrl"`
			AssemblyOptions []interface{} `json:"AssemblyOptions"`
		} `json:"Items"`
	} `json:"itemMetadata"`
}

type CustomerProfile struct {
	UserProfileID     string `json:"userProfileId"`
	ProfileProvider   string `json:"profileProvider"`
	AvailableAccounts []struct {
		AccountID          string   `json:"accountId"`
		PaymentSystem      string   `json:"paymentSystem"`
		PaymentSystemName  string   `json:"paymentSystemName"`
		CardNumber         string   `json:"cardNumber"`
		Bin                string   `json:"bin"`
		AvailableAddresses []string `json:"availableAddresses"`
	} `json:"availableAccounts"`
	AvailableAddresses []struct {
		AddressType    string      `json:"addressType"`
		ReceiverName   string      `json:"receiverName"`
		AddressID      string      `json:"addressId"`
		PostalCode     string      `json:"postalCode"`
		City           string      `json:"city"`
		State          string      `json:"state"`
		Country        string      `json:"country"`
		Street         string      `json:"street"`
		Number         string      `json:"number"`
		Neighborhood   string      `json:"neighborhood"`
		Complement     string      `json:"complement"`
		Reference      interface{} `json:"reference"`
		GeoCoordinates []float64   `json:"geoCoordinates"`
	} `json:"availableAddresses"`
	UserProfile struct {
		Email                    string      `json:"email"`
		FirstName                string      `json:"firstName"`
		LastName                 string      `json:"lastName"`
		Document                 string      `json:"document"`
		DocumentType             string      `json:"documentType"`
		Phone                    string      `json:"phone"`
		CorporateName            interface{} `json:"corporateName"`
		TradeName                interface{} `json:"tradeName"`
		CorporateDocument        interface{} `json:"corporateDocument"`
		StateInscription         interface{} `json:"stateInscription"`
		CorporatePhone           interface{} `json:"corporatePhone"`
		IsCorporate              bool        `json:"isCorporate"`
		ProfileCompleteOnLoading interface{} `json:"profileCompleteOnLoading"`
		ProfileErrorOnLoading    interface{} `json:"profileErrorOnLoading"`
		CustomerClass            interface{} `json:"customerClass"`
	} `json:"userProfile"`
}

type SuccessReponse struct {
	Message string `json:"message"`
	Order FinalReturnOrder `json:"order"`
}

type TransactionData struct {
	MerchantTransactions []struct {
		ID            string `json:"id"`
		TransactionID string `json:"transactionId"`
		MerchantName  string `json:"merchantName"`
		Payments      []struct {
			PaymentSystem          string      `json:"paymentSystem"`
			Bin                    interface{} `json:"bin"`
			AccountID              interface{} `json:"accountId"`
			TokenID                interface{} `json:"tokenId"`
			Value                  int         `json:"value"`
			ReferenceValue         int         `json:"referenceValue"`
			GiftCardRedemptionCode interface{} `json:"giftCardRedemptionCode"`
			GiftCardProvider       interface{} `json:"giftCardProvider"`
			GiftCardID             interface{} `json:"giftCardId"`
		} `json:"payments"`
	} `json:"merchantTransactions"`
	ReceiverURI                 string `json:"receiverUri"`
	GatewayCallbackTemplatePath string `json:"gatewayCallbackTemplatePath"`
}
*/