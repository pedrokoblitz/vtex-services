package vtex

import (
	"log"
	"net/http"
	"strings"
)

func apiPostOrderForm(payload []byte) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm"
	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("POST", url, reqPayload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiPostAddToCart(payload []byte, orderFormID string) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/" + orderFormID + "/items"
	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("POST", url, reqPayload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiPostRemoveFromCart(payload []byte, orderFormID string) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/" + orderFormID + "/items/update"
	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("POST", url, reqPayload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiPostAddCoupon(payload []byte, orderFormID string) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/" + orderFormID + "/coupons"
	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("POST", url, reqPayload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiGetZipcode(zipcode string) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/postal-code/BRA/" + zipcode
	req, err = http.NewRequest("GET", url, nil)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiPostShippingData(payload []byte, orderFormID string) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/" + orderFormID + "/attachments/shippingData"
	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("POST", url, reqPayload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiPostClientProfileData(payload []byte, orderFormID string) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/" + orderFormID + "/attachments/clientProfileData"
	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("POST", url, reqPayload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiPostSimulation(payload []byte) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForms/simulation"
	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("POST", url, reqPayload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiPostMarketingData(payload []byte, orderFormID string) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/" + orderFormID + "/attachments/marketingData"
	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("POST", url, reqPayload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiPostPaymentData(payload []byte, orderFormID string) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/" + orderFormID + "/attachments/paymentData"
	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("POST", url, reqPayload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiPostTransaction(payload []byte, orderFormID string) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/" + orderFormID + "/transaction"
	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("POST", url, reqPayload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}


func apiPostPayment(transactionID string, payload []byte) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "https://designupliving.vtexpayments.com.br/api/pub/transactions/" + transactionID + "/payments"

	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("POST", url, reqPayload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiPostProcessPayment(orderGroup string, cookie *http.Cookie) *http.Response {
	var req *http.Request
	var err error

	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/gatewayCallback/" + orderGroup
	req, err = http.NewRequest("POST", url, nil)

	req.AddCookie(cookie)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}


func apiGetProfile(email string) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/profiles?email=" + email
	req, err = http.NewRequest("GET", url, nil)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiPostCartSimulation(payload []byte) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/fulfillment/pvt/orderForms/simulation"
	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("POST", url, reqPayload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}
