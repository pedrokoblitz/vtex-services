package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"vtex"
)

func main() {

	r := mux.NewRouter()

	// CART
	// http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/3a7ffa4d90924d7682ee2a3e202bd865/items/update/ // remove cart
	r.HandleFunc("/api/checkout/pub/orderForm/{orderFormId}/items/update", vtex.PostRemoveFromCart).Methods("POST")
	// http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/3a7ffa4d90924d7682ee2a3e202bd865/items // add cart
	r.HandleFunc("/api/checkout/pub/orderForm/{orderFormId}/items", vtex.PostAddToCart).Methods("POST")

	// ORDER
	// http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/
	r.HandleFunc("/api/checkout/pub/orderForm", vtex.PostOrderForm).Methods("POST")
	// http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/ceb720a68e7c4667951450b810163f98/coupons
	r.HandleFunc("/api/checkout/pub/orderForm/{orderFormId}/coupon", vtex.PostAddCoupon).Methods("POST")
	// http://designupliving.vtexcommercestable.com.br/api/checkout/pub/postal-code/BRA/22260-100
	r.HandleFunc("/api/checkout/pub/postal-code/BRA/{zipcode}", vtex.GetZipcode).Methods("GET")
	// http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/{orderFormId}/attachments/shippingData
	r.HandleFunc("/api/checkout/pub/orderForm/{orderFormId}/attachments/shippingData", vtex.PostShippingData).Methods("POST")
	// http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForms/simulation?sc=1&rnbBehavior=0
	r.HandleFunc("/api/checkout/pub/orderForms/simulation", vtex.PostSimulation).Methods("POST")
	// http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/{orderFormId}/attachments/paymentData
	r.HandleFunc("/api/checkout/pub/orderForm/{orderFormId}/attachments/paymentData", vtex.PostPaymentData).Methods("POST")

	//
	r.HandleFunc("/api/checkout/pub/orderForm/{orderFormId}/attachments/clientProfileData", vtex.PostClientProfileData).Methods("POST")

	// PAYMENT
	// 	http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/{orderFormId}/transaction
	r.HandleFunc("/api/checkout/pub/orderForm/{orderFormId}/transaction", vtex.PostTransaction).Methods("POST")

	// https://taco.vtexpayments.com.br/split/v1075941Taco/payments
	// r.HandleFunc("/api/checkout/confirmation", vtex.).Methods("POST")

	// MISC
	// http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orderForm/3a7ffa4d90924d7682ee2a3e202bd865/attachments/marketingData
	r.HandleFunc("/api/checkout/pub/orderForm/{orderFormId}/attachments/marketingData", vtex.PostMarketingData).Methods("POST")

    r.HandleFunc("/api/profile/{email}", vtex.GetProfile).Methods("GET")

    r.HandleFunc("/api/cart/simulation", vtex.PostCartSimulation).Methods("POST")

	fmt.Println("Listening on 8081")
	log.Fatal(http.ListenAndServe(":8081", r))
}
