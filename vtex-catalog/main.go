package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"vtex"
)

func main() {
	r := mux.NewRouter()

	r.HandleFunc("/recommendation/sawsaw/{productID}", vtex.GetRecommendationSawSaw).Methods("GET")
	r.HandleFunc("/recommendation/sawbought/{productID}", vtex.GetRecommendationSawBought).Methods("GET")
	r.HandleFunc("/recommendation/boughtbought/{productID}", vtex.GetRecommendationBoughtBought).Methods("GET")
	r.HandleFunc("/recommendation/similar/{productID}", vtex.GetRecommendationSimilar).Methods("GET")
	
	r.HandleFunc("/products/search/{search}/page/{page}", vtex.GetSearchProducts).Methods("GET")

	r.HandleFunc("/products/GetIndexedInfo/{productID}", vtex.IndexedInfo).Methods("GET")
	r.HandleFunc("/products/ProductGet/{productID}", vtex.GetProductByID).Methods("GET")
	r.HandleFunc("/products/GetProductAndSkuIds/{categoryID}/{first}", vtex.GetProductsAndSkuIds).Methods("GET")
	r.HandleFunc("/products/{productID}/specification", vtex.GetProductSpecification).Methods("GET")
	r.HandleFunc("/products/productgetbyrefid/{refID}", vtex.GetProductByRefId).Methods("GET")
	r.HandleFunc("/products/variations/{productId}", vtex.GetProductVariations).Methods("GET")

	r.HandleFunc("/category/{levels}/tree", vtex.GetCateGoryTree).Methods("GET")
	r.HandleFunc("/category/{categoryID}", vtex.GetCategory).Methods("GET")

	r.HandleFunc("/sku/stockkeepingunitByProductId/{productID}", vtex.GetSkuListByProductID).Methods("GET")
	r.HandleFunc("/sku/stockkeepingunitids", vtex.GetListAllSkuIds).Methods("GET")
	r.HandleFunc("/sku/stockkeepingunitbyid/{skuId}", vtex.GetSku).Methods("GET")

	r.HandleFunc("/pricing/prices/{skuId}", vtex.GetPrice).Methods("GET")

    /*
	r.HandleFunc("/brand/{brandId}", vtex.GetBrand).Methods("GET")
	r.HandleFunc("/brand/list", vtex.GetBrandList).Methods("GET")

	r.HandleFunc("/review/GetProductRate/{productId}", vtex.GetReviewRateProduct).Methods("GET")
	r.HandleFunc("/sku/stockkeepingunitbyid/{skuId}", vtex.GetSku).Methods("GET")
	r.HandleFunc("/sku/stockkeepingunitidbyrefid/{refId}{", vtex.GetSkuIdByRefId).Methods("GET")
	r.HandleFunc("/sku/stockkeepingunitbyean/{ean}", vtex.GetSkuByEAN).Methods("GET")
	r.HandleFunc("/sku/stockkeepingunitbyalternateId/{alternateId}", vtex.GetSkuByAlternateId).Methods("GET")
	r.HandleFunc("/sku/stockkeepingunitidsbyrefids", vtex.PostSkuIdListByRefIdList).Methods("POST")
	r.HandleFunc("/sku/associateattachments", vtex.PostAssociateAttachmentsToSku).Methods("POST")

	r.HandleFunc("/saleschannel/list", vtex.GetSalesChannelList).Methods("GET")
	r.HandleFunc("/saleschannel/{salesChannelId}", vtex.GetSalesChannelById).Methods("GET")

	r.HandleFunc("/seller/{sellerId}", vtex.GetSellerById).Methods("GET")
	r.HandleFunc("/seller/{sellerId}", vtex.PostCreateSeller).Methods("POST")
	r.HandleFunc("/seller/{sellerId}", vtex.PutUpdateSeller).Methods("PUT")

	r.HandleFunc("/skuseller/{sellerID}/{sellerSkuID}", vtex.GetSkuSeller).Methods("GET")
	r.HandleFunc("/skuseller/remove/{sellerID}/{sellerSkuID}", vtex.PostDeleteSkuSellerAssociation).Methods("POST")
	r.HandleFunc("/skuseller/changenotification/{stockKeepingUnitID}", vtex.ChangeNotification).Methods("GET")

	r.HandleFunc("/specification/field/listByCategoryId/{CategoryId}", vtex.GetSpecificationsByCategoryId).Methods("GET")
	r.HandleFunc("/specification/field/listTreeByCategoryId/{categoryID}", vtex.GetSpecificationsTreeByCategoryId).Methods("GET")
	r.HandleFunc("/specification/fieldvalue/{fieldId}", vtex.GetSpecificationsValueByFieldId).Methods("GET")
	r.HandleFunc("/specification/fieldGet/{fieldId}", vtex.GetSpecificationsField).Methods("GET")
	r.HandleFunc("/specification/field", vtex.PostSpecificationsInsertField).Methods("POST")
	r.HandleFunc("/specification/field", vtex.PutSpecificationsInsertFieldUpdate).Methods("PUT")
	r.HandleFunc("/specification/fieldValue", vtex.GetSpecificationsGetFieldValue).Methods("GET")
	r.HandleFunc("/specification/fieldValue", vtex.PostSpecificationsInsertFieldValue).Methods("POST")
	r.HandleFunc("/specification/fieldValue", vtex.PutSpecificationsUpdateFieldValue).Methods("PUT")
	r.HandleFunc("/specification/groupGet/{groupID}", vtex.GetSpcificationsGroupGet).Methods("GET")
	r.HandleFunc("/specification/groupbycategory/{categoryID}", vtex.GetSpecificationsGroupListByCategory).Methods("GET")
	r.HandleFunc("/specification/group", vtex.PostSpecificationGroupInsert).Methods("POST")
	r.HandleFunc("/specification/group", vtex.PutSpecificationGroupUpdate).Methods("PUT")
    */
    
	/*
	   // faltou GetSellerList
	*/
	log.Println("Listening on 8080")
	log.Fatal(http.ListenAndServe(":8080", r))
}
