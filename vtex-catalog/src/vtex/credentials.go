package vtex

import (
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"encoding/json"
	"bytes"
)

const (
	key     = "vtexappkey-designupliving-BKDAFH"
	token   = "FOQHCNJDUEAMTZIBOWBCEYPNDELTWFKZETJPNLKTMRCFLYXELYUCXFQGZDEYCQHOUBIZPSOCMHQNVJBEHGIYLHCYDDQKHMRWJNLKQXPSJRQPPCKQBZQVHJCMYJVLBDVA"
	baseUrl = "https://designupliving.vtexcommercestable.com.br"
)

func doRequest(url string, method string, payload *strings.Reader) []byte {

	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		log.Println(err)
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	log.Println(string(body))

	if err != nil {
		panic(err)
	}

	return body
}


func doPriceRequest(url string, method string, payload *strings.Reader) []byte {
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		log.Println(err)
	}
	req.Header.Add("Accept", "application/vnd.vtex.pricing.v3+json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		panic(err)
	}

	return body
}

func doPostRequest(url string, message interface{}) {

	bytesRepresentation, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
	}

	payload := bytes.NewBuffer(bytesRepresentation)
	resp, err := http.Post(url, "application/json", payload)
	if err != nil {
		log.Fatalln(err)
	}

	var result map[string]interface{}

	json.NewDecoder(resp.Body).Decode(&result)

	log.Println("MESSAGE", message)

	log.Println("RESULT", result)
	log.Println("DATA", result["data"])
}

func doPutRequest(url string, message interface{}) []byte {

	bytesRepresentation, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
	}

	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(bytesRepresentation))

	if err != nil {
		log.Println(err)
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println(err)
	}
	
	return body
}
