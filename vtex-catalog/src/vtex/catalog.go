package vtex

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
    "io/ioutil"
    "encoding/json"
    "strconv"
)

///////// VTEX CATALOG API /////////////

func GetSearchProducts(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	search := params["search"]
	
	log.Println("term: " + search)

	page := params["page"]
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		log.Println(err)
	}

	perPage := 20
	start := ((pageInt - 1) * perPage) + 1
	end := start + (perPage - 1)

	startString := strconv.Itoa(start)
	endString := strconv.Itoa(end)

	url := baseUrl + "/api/catalog_system/pub/products/search/" + search + "?_from=" + startString + "&_to=" + endString
	// log.Println(url)

	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}


func GetRecommendationSawSaw(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productID := params["productID"]
	url := baseUrl + "/api/catalog_system/pub/products/crossselling/whosawalsosaw/" + productID
	// log.Println(url)
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetRecommendationSawBought(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productID := params["productID"]
	url := baseUrl + "/api/catalog_system/pub/products/crossselling/whosawalsobought/" + productID
	// log.Println(url)
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetRecommendationBoughtBought(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productID := params["productID"]
	url := baseUrl + "/api/catalog_system/pub/products/crossselling/whoboughtalsobought/" + productID
	// log.Println(url)
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetRecommendationSimilar(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productID := params["productID"]
	url := baseUrl + "/api/catalog_system/pub/products/crossselling/similars/" + productID
	// log.Println(url)
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetCategory(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	categoryID := params["categoryID"]
	url := baseUrl + "/api/catalog_system/pvt/category/" + categoryID
	// log.Println(url)
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetProductByID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productID := params["productID"]
	url := baseUrl + "/api/catalog_system/pvt/products/ProductGet/" + productID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetSkuListByProductID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productID := params["productID"]
	url := baseUrl + "/api/catalog_system/pvt/sku/stockkeepingunitByProductId/" + productID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetCateGoryTree(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	levels := params["levels"]
	url := baseUrl + "/api/catalog_system/pub/category/tree/" + levels
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetProductsAndSkuIds(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	categoryID := params["categoryId"]
	first := params["first"]
	firstIntValue, err := strconv.Atoi(first)
	if (err != nil) {
		panic(err)
	}
	lastIntValue := firstIntValue + 50
	last := strconv.Itoa(lastIntValue)
	url := baseUrl + "/api/catalog_system/pvt/products/GetProductAndSkuIds?categoryId=" + categoryID + "&_from=" + first + "&_to=" + last
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetListAllSkuIds(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	page := params["page"]
	pageSize := params["pageSize"]
	url := baseUrl + "/api/catalog_system/pvt/sku/stockkeepingunitids?page=" + page + "&pagesize=" + pageSize
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetBrand(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	brandID := params["brandId"]
	url := baseUrl + "/api/catalog_system/pvt/brand/" + brandID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetBrandList(w http.ResponseWriter, r *http.Request) {
	url := baseUrl + "/api/catalog_system/pvt/brand/list"
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetProductSpecification(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productID := params["productId"]
	url := baseUrl + "/api/catalog_system/pvt/products/" + productID + "/specification"
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func PostProductSpecification(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productID := params["productId"]

  body, err := ioutil.ReadAll(r.Body)
  if err != nil {
      panic(err)
  }
  log.Println("REQUEST BODY:", string(body))
  var s []Specification
  err = json.Unmarshal(body, &s)
  if err != nil {
      panic(err)
  }
  log.Println("STRUCTS:", s)

	url := baseUrl + "/api/catalog_system/pvt/products/" + productID + "/specification"
	resPayload := doRequest(url, "POST", nil)
  log.Println("PAYLOAD:", resPayload)
	w.Write(resPayload)
}

func GetProductByRefId(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	refID := params["refId"]
	url := baseUrl + "/api/catalog_system/pvt/products/productgetbyrefid/" + refID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetProductVariations(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productID := params["productId"]
	url := baseUrl + "/api/catalog_system/pub/products/variations/" + productID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetReviewRateProduct(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productID := params["productId"]
	url := baseUrl + "/api/catalog_system/pvt/review/GetProductRate/" + productID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetSku(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	skuID := params["skuId"]
	url := baseUrl + "/api/catalog_system/pvt/sku/stockkeepingunitbyid/" + skuID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetSkuIdByRefId(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	refID := params["refId"]
	url := baseUrl + "/api/catalog_system/pvt/sku/stockkeepingunitidbyrefid/" + refID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetSkuByEAN(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	ean := params["ean"]
	url := baseUrl + "/api/catalog_system/pvt/sku/stockkeepingunitbyean/" + ean
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetSkuByAlternateId(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	alternateID := params["alternateId"]
	url := baseUrl + "/api/catalog_system/pvt/sku/stockkeepingunitbyalternateId/" + alternateID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func PostSkuIdListByRefIdList(w http.ResponseWriter, r *http.Request) {
	url := baseUrl + "/api/catalog_system/pub/sku/stockkeepingunitidsbyrefids"
	resPayload := doRequest(url, "POST", nil)
	w.Write(resPayload)
}

func PostAssociateAttachmentsToSku(w http.ResponseWriter, r *http.Request) {
	url := baseUrl + "/api/catalog_system/pvt/sku/associateattachments"
	resPayload := doRequest(url, "POST", nil)
	w.Write(resPayload)
}

func GetSalesChannelList(w http.ResponseWriter, r *http.Request) {
	log.Println("HI!")
	url := baseUrl + "/api/catalog_system/pvt/saleschannel/list"
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetSalesChannelById(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	salesChannelID := params["salesChannelId"]
	url := baseUrl + "/api/catalog_system/pub/saleschannel/" + salesChannelID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetSellerById(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	sellerID := params["sellerId"]
	url := baseUrl + "/api/catalog_system/pvt/seller/" + sellerID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetSellerList(w http.ResponseWriter, r *http.Request) {
	url := baseUrl + "/api/catalog_system/pvt/seller/list?sc=1&sellerType=1&isBetterScope=false"
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func PostCreateSeller(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	sellerID := params["sellerId"]
	url := baseUrl + "/api/catalog_system/pvt/seller/" + sellerID
	resPayload := doRequest(url, "POST", nil)
	w.Write(resPayload)
}

func PutUpdateSeller(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	sellerID := params["sellerId"]
	url := baseUrl + "/api/catalog_system/pvt/seller/" + sellerID
	resPayload := doRequest(url, "PUT", nil)
	w.Write(resPayload)
}

func GetSkuSeller(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	sellerID := params["sellerId"]
	sellerSkuID := params["sellerSkuId"]
	url := baseUrl + "/api/catalog_system/pvt/skuseller/" + sellerID + "/" + sellerSkuID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func PostDeleteSkuSellerAssociation(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	sellerID := params["sellerId"]
	sellerSkuID := params["sellerSkuId"]
	url := baseUrl + "/api/catalog_system/pvt/skuseller/remove/" + sellerID + "/" + sellerSkuID
	resPayload := doRequest(url, "POST", nil)
	w.Write(resPayload)
}

func GetSpecificationsByCategoryId(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	categoryID := params["categoryId"]
	url := baseUrl + "/api/catalog_system/pub/specification/field/listByCategoryId/" + categoryID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetSpecificationsTreeByCategoryId(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	categoryID := params["categoryId"]
	url := baseUrl + "/api/catalog_system/pub/specification/field/listTreeByCategoryId/" + categoryID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetSpecificationsValueByFieldId(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	fieldID := params["fieldId"]
	url := baseUrl + "/api/catalog_system/pub/specification/fieldvalue/" + fieldID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetSpecificationsField(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	fieldID := params["fieldId"]
	url := baseUrl + "/api/catalog_system/pub/specification/fieldGet/" + fieldID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func PostSpecificationsInsertField(w http.ResponseWriter, r *http.Request) {
	url := baseUrl + "/api/catalog_system/pvt/specification/field"
	resPayload := doRequest(url, "POST", nil)
	w.Write(resPayload)
}

func PutSpecificationsInsertFieldUpdate(w http.ResponseWriter, r *http.Request) {
	url := baseUrl + "/api/catalog_system/pvt/specification/field"
	resPayload := doRequest(url, "PUT", nil)
	w.Write(resPayload)
}

func GetSpecificationsGetFieldValue(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	fieldValueID := params["fieldValueId"]
	url := baseUrl + "/api/catalog_system/pvt/specification/fieldValue/" + fieldValueID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func PostSpecificationsInsertFieldValue(w http.ResponseWriter, r *http.Request) {
	url := baseUrl + "/api/catalog_system/pvt/specification/fieldValue"
	resPayload := doRequest(url, "POST", nil)
	w.Write(resPayload)
}

func PutSpecificationsUpdateFieldValue(w http.ResponseWriter, r *http.Request) {
	url := baseUrl + "/api/catalog_system/pvt/specification/fieldValue"
	resPayload := doRequest(url, "PUT", nil)
	w.Write(resPayload)
}

func GetSpcificationsGroupGet(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	groupID := params["groupId"]
	url := baseUrl + "/api/catalog_system/pub/specification/groupGet/" + groupID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetSpecificationsGroupListByCategory(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	categoryID := params["categoryId"]
	url := baseUrl + "/api/catalog_system/pvt/specification/groupbycategory/" + categoryID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func PostSpecificationGroupInsert(w http.ResponseWriter, r *http.Request) {
	url := baseUrl + "/api/catalog_system/pvt/specification/group"
	resPayload := doRequest(url, "POST", nil)
	w.Write(resPayload)
}

func PutSpecificationGroupUpdate(w http.ResponseWriter, r *http.Request) {
	url := baseUrl + "/api/catalog_system/pvt/specification/group"
	resPayload := doRequest(url, "PUT", nil)
	w.Write(resPayload)
}

func ChangeNotification(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	stockKeepingUnitID := params["stockKeepingUnitId"]
	url := baseUrl + "/api/catalog_system/pvt/skuseller/changenotification/" + stockKeepingUnitID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func IndexedInfo(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productID := params["productId"]
	url := baseUrl + "/api/catalog_system/pvt/products/GetIndexedInfo/" + productID
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}
