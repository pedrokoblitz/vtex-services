package vtex

import (
	"time"
)

type CategoryTree []struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	HasChildren bool   `json:"hasChildren"`
	URL         string `json:"url"`
	Children    []struct {
		ID          int    `json:"id"`
		Name        string `json:"name"`
		HasChildren bool   `json:"hasChildren"`
		URL         string `json:"url"`
		Children    []struct {
			ID          int           `json:"id"`
			Name        string        `json:"name"`
			HasChildren bool          `json:"hasChildren"`
			URL         string        `json:"url"`
			Children    []interface{} `json:"children"`
		} `json:"children"`
	} `json:"children"`
}

type Sku struct {
	ID                 int         `json:"Id"`
	ProductID          int         `json:"ProductId"`
	NameComplete       string      `json:"NameComplete"`
	ProductName        string      `json:"ProductName"`
	ProductDescription string      `json:"ProductDescription"`
	SkuName            string      `json:"SkuName"`
	IsActive           bool        `json:"IsActive"`
	IsTransported      bool        `json:"IsTransported"`
	IsInventoried      bool        `json:"IsInventoried"`
	IsGiftCardRecharge bool        `json:"IsGiftCardRecharge"`
	ImageURL           string      `json:"ImageUrl"`
	DetailURL          string      `json:"DetailUrl"`
	CSCIdentification  interface{} `json:"CSCIdentification"`
	BrandID            string      `json:"BrandId"`
	BrandName          string      `json:"BrandName"`
	Dimension          struct {
		Cubicweight float64 `json:"cubicweight"`
		Height      int     `json:"height"`
		Length      int     `json:"length"`
		Weight      int     `json:"weight"`
		Width       int     `json:"width"`
	} `json:"Dimension"`
	RealDimension struct {
		RealCubicWeight float64 `json:"realCubicWeight"`
		RealHeight      int     `json:"realHeight"`
		RealLength      int     `json:"realLength"`
		RealWeight      int     `json:"realWeight"`
		RealWidth       int     `json:"realWidth"`
	} `json:"RealDimension"`
	ManufacturerCode string        `json:"ManufacturerCode"`
	IsKit            bool          `json:"IsKit"`
	KitItems         []interface{} `json:"KitItems"`
	Services         []interface{} `json:"Services"`
	Categories       []interface{} `json:"Categories"`
	Attachments      []struct {
		ID     int      `json:"Id"`
		Name   string   `json:"Name"`
		Keys   []string `json:"Keys"`
		Fields []struct {
			FieldName    string `json:"FieldName"`
			MaxCaracters string `json:"MaxCaracters"`
			DomainValues string `json:"DomainValues"`
		} `json:"Fields"`
		IsActive   bool `json:"IsActive"`
		IsRequired bool `json:"IsRequired"`
	} `json:"Attachments"`
	Collections []interface{} `json:"Collections"`
	SkuSellers  []struct {
		SellerID                    string `json:"SellerId"`
		StockKeepingUnitID          int    `json:"StockKeepingUnitId"`
		SellerStockKeepingUnitID    string `json:"SellerStockKeepingUnitId"`
		IsActive                    bool   `json:"IsActive"`
		FreightCommissionPercentage int    `json:"FreightCommissionPercentage"`
		ProductCommissionPercentage int    `json:"ProductCommissionPercentage"`
	} `json:"SkuSellers"`
	SalesChannels []int `json:"SalesChannels"`
	Images        []struct {
		ImageURL  string `json:"ImageUrl"`
		ImageName string `json:"ImageName"`
		FileID    int    `json:"FileId"`
	} `json:"Images"`
	SkuSpecifications []struct {
		FieldID       int      `json:"FieldId"`
		FieldName     string   `json:"FieldName"`
		FieldValueIds []int    `json:"FieldValueIds"`
		FieldValues   []string `json:"FieldValues"`
	} `json:"SkuSpecifications"`
	ProductSpecifications []struct {
		FieldID       int      `json:"FieldId"`
		FieldName     string   `json:"FieldName"`
		FieldValueIds []int    `json:"FieldValueIds"`
		FieldValues   []string `json:"FieldValues"`
	} `json:"ProductSpecifications"`
	ProductClustersIds      string      `json:"ProductClustersIds"`
	ProductCategoryIds      string      `json:"ProductCategoryIds"`
	ProductGlobalCategoryID interface{} `json:"ProductGlobalCategoryId"`
	ProductCategories       struct {
		Num59 string `json:"59"`
	} `json:"ProductCategories"`
	CommercialConditionID int `json:"CommercialConditionId"`
	RewardValue           int `json:"RewardValue"`
	AlternateIds          struct {
		Ean   string `json:"Ean"`
		RefID string `json:"RefId"`
	} `json:"AlternateIds"`
	AlternateIDValues    []string    `json:"AlternateIdValues"`
	EstimatedDateArrival interface{} `json:"EstimatedDateArrival"`
	MeasurementUnit      string      `json:"MeasurementUnit"`
	UnitMultiplier       int         `json:"UnitMultiplier"`
	InformationSource    string      `json:"InformationSource"`
	ModalType            interface{} `json:"ModalType"`
}

type Product struct {
	ID                     int         `json:"Id"`
	Name                   string      `json:"Name"`
	DepartmentID           int         `json:"DepartmentId"`
	CategoryID             int         `json:"CategoryId"`
	BrandID                int         `json:"BrandId"`
	LinkID                 string      `json:"LinkId"`
	RefID                  string      `json:"RefId"`
	IsVisible              bool        `json:"IsVisible"`
	Description            string      `json:"Description"`
	DescriptionShort       string      `json:"DescriptionShort"`
	ReleaseDate            string      `json:"ReleaseDate"`
	KeyWords               string      `json:"KeyWords"`
	Title                  string      `json:"Title"`
	IsActive               bool        `json:"IsActive"`
	TaxCode                string      `json:"TaxCode"`
	MetaTagDescription     string      `json:"MetaTagDescription"`
	SupplierID             int         `json:"SupplierId"`
	ShowWithoutStock       bool        `json:"ShowWithoutStock"`
	ListStoreID            []int       `json:"ListStoreId"`
	AdWordsRemarketingCode interface{} `json:"AdWordsRemarketingCode"`
	LomadeeCampaignCode    interface{} `json:"LomadeeCampaignCode"`
}

type Category struct {
	ParentID    interface{} `json:"parentId"`
	ID          int         `json:"id"`
	Name        string      `json:"name"`
	HasChildren bool        `json:"hasChildren"`
	URL         interface{} `json:"url"`
	Children    interface{} `json:"children"`
}

type Brand struct {
	ID                 int    `json:"id"`
	Name               string `json:"name"`
	IsActive           bool   `json:"isActive"`
	Title              string `json:"title"`
	MetaTagDescription string `json:"metaTagDescription"`
}

type Specification struct {
	Name               string `json:"Name"`
	CategoryID         int    `json:"CategoryId"`
	FieldID            int    `json:"FieldId"`
	IsActive           bool   `json:"IsActive"`
	IsStockKeepingUnit bool   `json:"IsStockKeepingUnit"`
}

type Seller struct {
	SellerID                    string `json:"SellerId"`
	Name                        string `json:"Name"`
	Email                       string `json:"Email"`
	Description                 string `json:"Description"`
	ExchangeReturnPolicy        string `json:"ExchangeReturnPolicy"`
	DeliveryPolicy              string `json:"DeliveryPolicy"`
	UseHybridPaymentOptions     bool   `json:"UseHybridPaymentOptions"`
	UserName                    string `json:"UserName"`
	Password                    string `json:"Password"`
	SecutityPrivacyPolicy       string `json:"SecutityPrivacyPolicy"`
	CNPJ                        string `json:"CNPJ"`
	CSCIdentification           string `json:"CSCIdentification"`
	ArchiveID                   int    `json:"ArchiveId"`
	UrlLogo                     string `json:"UrlLogo"`
	ProductCommissionPercentage float64  `json:"ProductCommissionPercentage"`
	FreightCommissionPercentage float64  `json:"FreightCommissionPercentage"`
	FulfillmentEndpoint         string `json:"FulfillmentEndpoint"`
	CatalogSystemEndpoint       string `json:"CatalogSystemEndpoint"`
	IsActive                    bool   `json:"IsActive"`
	FulfillmentSellerID         string `json:"FulfillmentSellerId"`
	SellerType                  int    `json:"SellerType"`
	IsBetterScope               int    `json:"IsBetterScope"`
}

type SkuSeller struct {
	IsPersisted              bool        `json:"IsPersisted"`
	IsRemoved                bool        `json:"IsRemoved"`
	SkuSellerID              int         `json:"SkuSellerId"`
	SellerID                 string      `json:"SellerId"`
	StockKeepingUnitID       int         `json:"StockKeepingUnitId"`
	SellerStockKeepingUnitID string      `json:"SellerStockKeepingUnitId"`
	IsActive                 bool        `json:"IsActive"`
	UpdateDate               string      `json:"UpdateDate"`
	RequestedUpdateDate      interface{} `json:"RequestedUpdateDate"`
}

type SpecificationField struct {
	Name                 string `json:"Name"`
	CategoryID           int    `json:"CategoryId"`
	FieldID              int    `json:"FieldId"`
	IsActive             bool   `json:"IsActive"`
	IsRequired           bool   `json:"IsRequired"`
	FieldTypeID          int    `json:"FieldTypeId"`
	FieldValueID         int    `json:"FieldValueId"`
	Description          string `json:"Description"`
	IsStockKeepingUnit   bool   `json:"IsStockKeepingUnit"`
	IsFilter             bool   `json:"IsFilter"`
	IsOnProductDetails   bool   `json:"IsOnProductDetails"`
	Position             int    `json:"Position"`
	IsWizard             bool   `json:"IsWizard"`
	IsTopMenuLinkActive  bool   `json:"IsTopMenuLinkActive"`
	IsSideMenuLinkActive bool   `json:"IsSideMenuLinkActive"`
	DefaultValue         string `json:"DefaultValue"`
	FieldGroupID         int    `json:"FieldGroupId"`
	FieldGroupName       string `json:"FieldGroupName"`
}

type SpecificationsGroup struct {
	CategoryID interface{} `json:"CategoryId"`
	ID         int         `json:"Id"`
	Name       string      `json:"Name"`
	Position   int         `json:"Position"`
}



type Recommendation struct {
	ProductID          string      `json:"productId"`
	ProductName        string      `json:"productName"`
	Brand              string      `json:"brand"`
	BrandID            int         `json:"brandId"`
	BrandImageURL      interface{} `json:"brandImageUrl"`
	LinkText           string      `json:"linkText"`
	ProductReference   string      `json:"productReference"`
	CategoryID         string      `json:"categoryId"`
	ProductTitle       string      `json:"productTitle"`
	MetaTagDescription string      `json:"metaTagDescription"`
	ClusterHighlights  struct {
	} `json:"clusterHighlights"`
	SearchableClusters map[string]interface{} `json:"searchableClusters,omitempty"`
	Categories    []string `json:"categories"`
	CategoriesIds []string `json:"categoriesIds"`
	Link          string   `json:"link"`
	Description   string   `json:"description"`
	Items         []struct {
		ItemID         string `json:"itemId"`
		Name           string `json:"name"`
		NameComplete   string `json:"nameComplete"`
		ComplementName string `json:"complementName"`
		Ean            string `json:"ean"`
		ReferenceID    []struct {
			Key   string `json:"Key"`
			Value string `json:"Value"`
		} `json:"referenceId"`
		MeasurementUnit string      `json:"measurementUnit"`
		UnitMultiplier  int         `json:"unitMultiplier"`
		ModalType       interface{} `json:"modalType"`
		IsKit           bool        `json:"isKit"`
		Images          []struct {
			ImageID    string `json:"imageId"`
			ImageLabel string `json:"imageLabel"`
			ImageTag   string `json:"imageTag"`
			ImageURL   string `json:"imageUrl"`
			ImageText  string `json:"imageText"`
		} `json:"images"`
		Sellers []struct {
			SellerID        string `json:"sellerId"`
			SellerName      string `json:"sellerName"`
			AddToCartLink   string `json:"addToCartLink"`
			SellerDefault   bool   `json:"sellerDefault"`
			CommertialOffer struct {
				DeliverySLASamplesPerRegion struct {
					Num0 struct {
						DeliverySLAPerTypes []interface{} `json:"DeliverySlaPerTypes"`
						Region              interface{}   `json:"Region"`
					} `json:"0"`
				} `json:"DeliverySlaSamplesPerRegion"`
				Installments []struct {
					Value                      float64 `json:"Value"`
					InterestRate               int     `json:"InterestRate"`
					TotalValuePlusInterestRate float64 `json:"TotalValuePlusInterestRate"`
					NumberOfInstallments       int     `json:"NumberOfInstallments"`
					PaymentSystemName          string  `json:"PaymentSystemName"`
					PaymentSystemGroupName     string  `json:"PaymentSystemGroupName"`
					Name                       string  `json:"Name"`
				} `json:"Installments"`
				DiscountHighLight      []interface{} `json:"DiscountHighLight"`
				GiftSkuIds             []interface{} `json:"GiftSkuIds"`
				Teasers                []interface{} `json:"Teasers"`
				BuyTogether            []interface{} `json:"BuyTogether"`
				ItemMetadataAttachment []interface{} `json:"ItemMetadataAttachment"`
				Price                  float64       `json:"Price"`
				ListPrice              float64       `json:"ListPrice"`
				PriceWithoutDiscount   float64       `json:"PriceWithoutDiscount"`
				RewardValue            int           `json:"RewardValue"`
				PriceValidUntil        time.Time     `json:"PriceValidUntil"`
				AvailableQuantity      int           `json:"AvailableQuantity"`
				Tax                    int           `json:"Tax"`
				DeliverySLASamples     []struct {
					DeliverySLAPerTypes []interface{} `json:"DeliverySlaPerTypes"`
					Region              interface{}   `json:"Region"`
				} `json:"DeliverySlaSamples"`
				GetInfoErrorMessage            interface{} `json:"GetInfoErrorMessage"`
				CacheVersionUsedToCallCheckout string      `json:"CacheVersionUsedToCallCheckout"`
			} `json:"commertialOffer"`
		} `json:"sellers"`
		Videos []interface{} `json:"Videos"`
	} `json:"items"`
	ProductClusters map[string]interface{} `json:"productClusters,omitempty"`
}
