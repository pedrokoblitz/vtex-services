package vtex

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

// 9997
// https://api.vtex.com/{{account}}/pricing/prices/{{itemId}}/fixed/{{priceTableId}}
// https://api.vtex.com/{{account}}/pricing/prices/{{itemId}}/computed/{{priceTableId}}?categoryIds=[{{categoryId}}]&brandId={{brandId}}&quantity={{quantity}}

func GetPrice(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	skuID := params["skuId"]
	url := "https://api.vtex.com/designupliving/pricing/prices/" + skuID
	log.Println(url)
	resPayload := doPriceRequest(url, "GET", nil)
	w.Write(resPayload)
}

/*
func GetFixedPriceTable(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	categoryID := params["categoryID"]
	url := "https://api.vtex.com/designupliving/"
	log.Println(url)
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}

func GetComputedPriceTable(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	categoryID := params["categoryID"]
	url := baseUrl + "/api/catalog_system/pvt/category/" + categoryID
	log.Println(url)
	resPayload := doRequest(url, "GET", nil)
	w.Write(resPayload)
}
*/