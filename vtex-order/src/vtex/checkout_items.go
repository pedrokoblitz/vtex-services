package vtex

import (
	"time"
)

type Sla struct {
	ItemID                   string  `json:"itemId"`
	Quantity                 int     `json:"quantity"`
	AvailabilityQuantity     int     `json:"availabilityQuantity"`
	SalesChannel             string  `json:"salesChannel"`
	SLAType                  string  `json:"slaType"`
	SLATypeName              string  `json:"slaTypeName"`
	FreightTableName         string  `json:"freightTableName"`
	FreightTableID           string  `json:"freightTableId"`
	ListPrice                float64 `json:"listPrice"`
	TransitTime              string  `json:"transitTime"`
	DockTime                 string  `json:"dockTime"`
	TimeToDockPlusDockTime   string  `json:"timeToDockPlusDockTime"`
	AditionalTimeBlockedDays string  `json:"aditionalTimeBlockedDays"`
	TotalTime                string  `json:"totalTime"`
	DeliveryWindows          []struct {
		StartDateUtc time.Time `json:"startDateUtc"`
		EndDateUtc   time.Time `json:"endDateUtc"`
		ListPrice    int       `json:"listPrice"`
	} `json:"deliveryWindows"`
	WareHouseID string `json:"wareHouseId"`
	DockID      string `json:"dockId"`
	Location    struct {
		ZipCode         string      `json:"zipCode"`
		Country         string      `json:"country"`
		DeliveryPointID interface{} `json:"deliveryPointId"`
		Point           interface{} `json:"point"`
		InStore         struct {
			IsCheckedIn bool   `json:"IsCheckedIn"`
			StoreID     string `json:"StoreId"`
		} `json:"inStore"`
	} `json:"location"`
	DeliveryOnWeekends bool          `json:"deliveryOnWeekends"`
	CarrierSchedule    []interface{} `json:"carrierSchedule"`
	RestrictedFreight  interface{}   `json:"restrictedFreight"`
	Coordinates        interface{}   `json:"coordinates"`
	PickupStoreInfo    interface{}   `json:"pickupStoreInfo"`
}


type FreightValue struct {
	ZipCodeStart          string        `json:"zipCodeStart"`
	ZipCodeEnd            string        `json:"zipCodeEnd"`
	WeightStart           int           `json:"weightStart"`
	WeightEnd             int           `json:"weightEnd"`
	AbsoluteMoneyCost     int           `json:"absoluteMoneyCost"`
	PricePercent          int           `json:"pricePercent"`
	PricePercentByWeight  int           `json:"pricePercentByWeight"`
	MaxVolume             int           `json:"maxVolume"`
	TimeCost              string        `json:"timeCost"`
	Country               string        `json:"country"`
	OperationType         int           `json:"operationType"`
	RestrictedFreights    []interface{} `json:"restrictedFreights"`
	Polygon               string        `json:"polygon"`
	MinimumValueInsurance int           `json:"minimumValueInsurance"`
}

type Item struct {
	ID          string        `json:"id"`
	Quantity    int           `json:"quantity"`
	Seller      string        `json:"seller"`
	Price       int           `json:"price"`
	RewardValue int           `json:"rewardValue"`
	Offerings   []interface{} `json:"offerings"`
	PriceTags   []interface{} `json:"priceTags"`
	IsGift      bool          `json:"isGift"`
}

type ClientProfileData struct {
	Email             string      `json:"email"`
	FirstName         string      `json:"firstName"`
	LastName          string      `json:"lastName"`
	Document          string      `json:"document"`
	DocumentType      string      `json:"documentType"`
	Phone             string      `json:"phone"`
	CorporateName     interface{} `json:"corporateName"`
	TradeName         interface{} `json:"tradeName"`
	CorporateDocument interface{} `json:"corporateDocument"`
	StateInscription  interface{} `json:"stateInscription"`
	CorporatePhone    interface{} `json:"corporatePhone"`
	IsCorporate       bool        `json:"isCorporate"`
}

type Address struct {
	AddressType    string        `json:"addressType"`
	ReceiverName   string        `json:"receiverName"`
	PostalCode     string        `json:"postalCode"`
	City           string        `json:"city"`
	State          string        `json:"state"`
	Country        string        `json:"country"`
	Street         string        `json:"street"`
	Number         string        `json:"number"`
	Neighborhood   string        `json:"neighborhood"`
	Complement     string        `json:"complement"`
	Reference      interface{}   `json:"reference"`
	GeoCoordinates []interface{} `json:"geoCoordinates"`
}

type LogisticsInfo struct {
	ItemIndex   int    `json:"itemIndex"`
	SelectedSLA string `json:"selectedSla"`
	Price       int    `json:"price"`
}

type ShippingData struct {
	ID            string          `json:"id"`
	Address       Address         `json:"address"`
	LogisticsInfo []LogisticsInfo `json:"logisticsInfo"`
}

type Payment struct {
	PaymentSystem  string `json:"paymentSystem"`
	ReferenceValue int    `json:"referenceValue"`
	Value          int    `json:"value"`
	Installments   int    `json:"installments"`
}

type PaymentData struct {
	ID       string    `json:"id"`
	Payments []Payment `json:"payments"`
}

type MarketingData struct {
	Coupon string `json:"coupon"`
}

type Order struct {
	Items             []Item            `json:"items"`
	ClientProfileData ClientProfileData `json:"clientProfileData"`
	ShippingData      ShippingData      `json:"shippingData"`
	PaymentData       PaymentData       `json:"paymentData"`
}

type FinalOrder struct {
	Items             []Item            `json:"items"`
	ClientProfileData ClientProfileData `json:"clientProfileData"`
	ShippingData      ShippingData      `json:"shippingData"`
	PaymentData       PaymentData       `json:"paymentData"`
	MarketingData     MarketingData     `json:"marketingData"`
}

type CcField struct {
	HolderName     string `json:"holderName"`
	CardNumber     string `json:"cardNumber"`
	ValidationCode string `json:"validationCode"`
	DueDate        string `json:"dueDate"`
	AddressID      string `json:"addressId"`
}

type Transaction struct {
	ID           string `json:"id"`
	MerchantName string `json:"merchantName"`
}

type PaymentInfo struct {
	PaymentSystem            string    `json:"paymentSystem"`
	PaymentSystemName        string `json:"paymentSystemName"`
	Group                    string `json:"group"`
	Installments             int    `json:"installments"`
	InstallmentsInterestRate int    `json:"installmentsInterestRate"`
	InstallmentsValue        int    `json:"installmentsValue"`
	Value                    int    `json:"value"`
	ReferenceValue           int    `json:"referenceValue"`
	Fields                   CcField `json:"fields"`
	Transaction Transaction `json:"transaction"`
	CurrencyCode string `json:"currencyCode"`
}

type CartItem struct {
	ID          string        `json:"id"`
	Quantity    int           `json:"quantity"`
	Seller      string        `json:"seller"`
	Price       int           `json:"price"`
}

type Cart struct {
	OrderItems []CartItem `json:"OrderItems"`
}

type PaymentResponse struct {
	PaymentSystem          string      `json:"paymentSystem"`
	Bin                    interface{} `json:"bin"`
	AccountID              interface{} `json:"accountId"`
	TokenID                interface{} `json:"tokenId"`
	Value                  int         `json:"value"`
	ReferenceValue         int         `json:"referenceValue"`
	GiftCardRedemptionCode interface{} `json:"giftCardRedemptionCode"`
	GiftCardProvider       interface{} `json:"giftCardProvider"`
	GiftCardID             interface{} `json:"giftCardId"`
}

type MerchantTransactionResponse struct {
	ID            string `json:"id"`
	TransactionID string `json:"transactionId"`
	MerchantName  string `json:"merchantName"`
	Payments      []PaymentResponse `json:"payments"`
}

type TransactionData struct {
	MerchantTransactions []struct {
		ID            string `json:"id"`
		TransactionID string `json:"transactionId"`
		MerchantName  string `json:"merchantName"`
		Payments      []struct {
			PaymentSystem          string      `json:"paymentSystem"`
			Bin                    interface{} `json:"bin"`
			AccountID              interface{} `json:"accountId"`
			TokenID                interface{} `json:"tokenId"`
			Value                  int         `json:"value"`
			ReferenceValue         int         `json:"referenceValue"`
			GiftCardRedemptionCode interface{} `json:"giftCardRedemptionCode"`
			GiftCardProvider       interface{} `json:"giftCardProvider"`
			GiftCardID             interface{} `json:"giftCardId"`
		} `json:"payments"`
	} `json:"merchantTransactions"`
	ReceiverURI                 string `json:"receiverUri"`
	GatewayCallbackTemplatePath string `json:"gatewayCallbackTemplatePath"`
}

type OrderItemResponse struct {

}

type OrderErrorReponse struct {
	OrderForm struct {
		OrderFormID            string      `json:"orderFormId"`
		SalesChannel           string      `json:"salesChannel"`
		LoggedIn               bool        `json:"loggedIn"`
		IsCheckedIn            bool        `json:"isCheckedIn"`
		StoreID                interface{} `json:"storeId"`
		CheckedInPickupPointID interface{} `json:"checkedInPickupPointId"`
		AllowManualPrice       bool        `json:"allowManualPrice"`
		CanEditData            bool        `json:"canEditData"`
		UserProfileID          string      `json:"userProfileId"`
		UserType               interface{} `json:"userType"`
		IgnoreProfileData      bool        `json:"ignoreProfileData"`
		Value                  int         `json:"value"`
		Messages               []struct {
			Code   interface{} `json:"code"`
			Text   string      `json:"text"`
			Status string      `json:"status"`
			Fields struct {
			} `json:"fields"`
		} `json:"messages"`
		Items []struct {
			UniqueID              string        `json:"uniqueId"`
			ID                    string        `json:"id"`
			ProductID             string        `json:"productId"`
			RefID                 string        `json:"refId"`
			Ean                   string        `json:"ean"`
			Name                  string        `json:"name"`
			SkuName               string        `json:"skuName"`
			ModalType             interface{}   `json:"modalType"`
			ParentItemIndex       interface{}   `json:"parentItemIndex"`
			ParentAssemblyBinding interface{}   `json:"parentAssemblyBinding"`
			Assemblies            []interface{} `json:"assemblies"`
			PriceValidUntil       time.Time     `json:"priceValidUntil"`
			Tax                   int           `json:"tax"`
			Price                 int           `json:"price"`
			ListPrice             int           `json:"listPrice"`
			ManualPrice           interface{}   `json:"manualPrice"`
			SellingPrice          int           `json:"sellingPrice"`
			RewardValue           int           `json:"rewardValue"`
			IsGift                bool          `json:"isGift"`
			AdditionalInfo        struct {
				BrandName      string      `json:"brandName"`
				BrandID        string      `json:"brandId"`
				OfferingInfo   interface{} `json:"offeringInfo"`
				OfferingType   interface{} `json:"offeringType"`
				OfferingTypeID interface{} `json:"offeringTypeId"`
			} `json:"additionalInfo"`
			PreSaleDate        interface{} `json:"preSaleDate"`
			ProductCategoryIds string      `json:"productCategoryIds"`
			ProductCategories  struct {
				Num8   string `json:"8"`
				Num13  string `json:"13"`
				Num138 string `json:"138"`
			} `json:"productCategories"`
			Quantity            int           `json:"quantity"`
			Seller              string        `json:"seller"`
			SellerChain         []string      `json:"sellerChain"`
			ImageURL            string        `json:"imageUrl"`
			DetailURL           string        `json:"detailUrl"`
			Components          []interface{} `json:"components"`
			BundleItems         []interface{} `json:"bundleItems"`
			Attachments         []interface{} `json:"attachments"`
			AttachmentOfferings []interface{} `json:"attachmentOfferings"`
			Offerings           []interface{} `json:"offerings"`
			PriceTags           []interface{} `json:"priceTags"`
			Availability        string        `json:"availability"`
			MeasurementUnit     string        `json:"measurementUnit"`
			UnitMultiplier      int           `json:"unitMultiplier"`
		} `json:"items"`
		SelectableGifts []interface{} `json:"selectableGifts"`
		Totalizers      []struct {
			ID    string `json:"id"`
			Name  string `json:"name"`
			Value int    `json:"value"`
		} `json:"totalizers"`
		ShippingData struct {
			Address struct {
				AddressType    string    `json:"addressType"`
				ReceiverName   string    `json:"receiverName"`
				AddressID      string    `json:"addressId"`
				PostalCode     string    `json:"postalCode"`
				City           string    `json:"city"`
				State          string    `json:"state"`
				Country        string    `json:"country"`
				Street         string    `json:"street"`
				Number         string    `json:"number"`
				Neighborhood   string    `json:"neighborhood"`
				Complement     string    `json:"complement"`
				Reference      string    `json:"reference"`
				GeoCoordinates []float64 `json:"geoCoordinates"`
			} `json:"address"`
			LogisticsInfo []struct {
				ItemIndex               int    `json:"itemIndex"`
				SelectedSLA             string `json:"selectedSla"`
				SelectedDeliveryChannel string `json:"selectedDeliveryChannel"`
				AddressID               string `json:"addressId"`
				Slas                    []struct {
					ID              string `json:"id"`
					DeliveryChannel string `json:"deliveryChannel"`
					Name            string `json:"name"`
					DeliveryIds     []struct {
						CourierID   string `json:"courierId"`
						WarehouseID string `json:"warehouseId"`
						DockID      string `json:"dockId"`
						CourierName string `json:"courierName"`
						Quantity    int    `json:"quantity"`
					} `json:"deliveryIds"`
					ShippingEstimate         string        `json:"shippingEstimate"`
					ShippingEstimateDate     interface{}   `json:"shippingEstimateDate"`
					LockTTL                  interface{}   `json:"lockTTL"`
					AvailableDeliveryWindows []interface{} `json:"availableDeliveryWindows"`
					DeliveryWindow           interface{}   `json:"deliveryWindow"`
					Price                    int           `json:"price"`
					ListPrice                int           `json:"listPrice"`
					Tax                      int           `json:"tax"`
					PickupStoreInfo          struct {
						IsPickupStore  bool        `json:"isPickupStore"`
						FriendlyName   interface{} `json:"friendlyName"`
						Address        interface{} `json:"address"`
						AdditionalInfo interface{} `json:"additionalInfo"`
						DockID         interface{} `json:"dockId"`
					} `json:"pickupStoreInfo"`
					PickupPointID  interface{} `json:"pickupPointId"`
					PickupDistance int         `json:"pickupDistance"`
					PolygonName    interface{} `json:"polygonName"`
				} `json:"slas"`
				ShipsTo          []string `json:"shipsTo"`
				ItemID           string   `json:"itemId"`
				DeliveryChannels []struct {
					ID string `json:"id"`
				} `json:"deliveryChannels"`
			} `json:"logisticsInfo"`
			SelectedAddresses []struct {
				AddressType    string    `json:"addressType"`
				ReceiverName   string    `json:"receiverName"`
				AddressID      string    `json:"addressId"`
				PostalCode     string    `json:"postalCode"`
				City           string    `json:"city"`
				State          string    `json:"state"`
				Country        string    `json:"country"`
				Street         string    `json:"street"`
				Number         string    `json:"number"`
				Neighborhood   string    `json:"neighborhood"`
				Complement     string    `json:"complement"`
				Reference      string    `json:"reference"`
				GeoCoordinates []float64 `json:"geoCoordinates"`
			} `json:"selectedAddresses"`
			AvailableAddresses []struct {
				AddressType    string    `json:"addressType"`
				ReceiverName   string    `json:"receiverName"`
				AddressID      string    `json:"addressId"`
				PostalCode     string    `json:"postalCode"`
				City           string    `json:"city"`
				State          string    `json:"state"`
				Country        string    `json:"country"`
				Street         string    `json:"street"`
				Number         string    `json:"number"`
				Neighborhood   string    `json:"neighborhood"`
				Complement     string    `json:"complement"`
				Reference      string    `json:"reference"`
				GeoCoordinates []float64 `json:"geoCoordinates"`
			} `json:"availableAddresses"`
			PickupPoints []struct {
				FriendlyName string `json:"friendlyName"`
				Address      struct {
					AddressType    string      `json:"addressType"`
					ReceiverName   interface{} `json:"receiverName"`
					AddressID      string      `json:"addressId"`
					PostalCode     string      `json:"postalCode"`
					City           string      `json:"city"`
					State          string      `json:"state"`
					Country        string      `json:"country"`
					Street         string      `json:"street"`
					Number         string      `json:"number"`
					Neighborhood   string      `json:"neighborhood"`
					Complement     string      `json:"complement"`
					Reference      interface{} `json:"reference"`
					GeoCoordinates []float64   `json:"geoCoordinates"`
				} `json:"address"`
				AdditionalInfo string `json:"additionalInfo"`
				ID             string `json:"id"`
				BusinessHours  []struct {
					DayOfWeek   int    `json:"DayOfWeek"`
					OpeningTime string `json:"OpeningTime"`
					ClosingTime string `json:"ClosingTime"`
				} `json:"businessHours"`
			} `json:"pickupPoints"`
		} `json:"shippingData"`
		ClientProfileData struct {
			Email                    string      `json:"email"`
			FirstName                string      `json:"firstName"`
			LastName                 string      `json:"lastName"`
			Document                 string      `json:"document"`
			DocumentType             string      `json:"documentType"`
			Phone                    string      `json:"phone"`
			CorporateName            interface{} `json:"corporateName"`
			TradeName                interface{} `json:"tradeName"`
			CorporateDocument        interface{} `json:"corporateDocument"`
			StateInscription         interface{} `json:"stateInscription"`
			CorporatePhone           interface{} `json:"corporatePhone"`
			IsCorporate              bool        `json:"isCorporate"`
			ProfileCompleteOnLoading interface{} `json:"profileCompleteOnLoading"`
			ProfileErrorOnLoading    interface{} `json:"profileErrorOnLoading"`
			CustomerClass            interface{} `json:"customerClass"`
		} `json:"clientProfileData"`
		PaymentData struct {
			InstallmentOptions []struct {
				PaymentSystem    string      `json:"paymentSystem"`
				Bin              interface{} `json:"bin"`
				PaymentName      interface{} `json:"paymentName"`
				PaymentGroupName interface{} `json:"paymentGroupName"`
				Value            int         `json:"value"`
				Installments     []struct {
					Count                      int  `json:"count"`
					HasInterestRate            bool `json:"hasInterestRate"`
					InterestRate               int  `json:"interestRate"`
					Value                      int  `json:"value"`
					Total                      int  `json:"total"`
					SellerMerchantInstallments []struct {
						ID              string `json:"id"`
						Count           int    `json:"count"`
						HasInterestRate bool   `json:"hasInterestRate"`
						InterestRate    int    `json:"interestRate"`
						Value           int    `json:"value"`
						Total           int    `json:"total"`
					} `json:"sellerMerchantInstallments"`
				} `json:"installments"`
			} `json:"installmentOptions"`
			PaymentSystems []struct {
				ID        int    `json:"id"`
				Name      string `json:"name"`
				GroupName string `json:"groupName"`
				Validator struct {
					Regex             interface{} `json:"regex"`
					Mask              interface{} `json:"mask"`
					CardCodeRegex     interface{} `json:"cardCodeRegex"`
					CardCodeMask      interface{} `json:"cardCodeMask"`
					Weights           interface{} `json:"weights"`
					UseCvv            bool        `json:"useCvv"`
					UseExpirationDate bool        `json:"useExpirationDate"`
					UseCardHolderName bool        `json:"useCardHolderName"`
					UseBillingAddress bool        `json:"useBillingAddress"`
				} `json:"validator"`
				StringID               string      `json:"stringId"`
				Template               string      `json:"template"`
				RequiresDocument       bool        `json:"requiresDocument"`
				IsCustom               bool        `json:"isCustom"`
				Description            interface{} `json:"description"`
				RequiresAuthentication bool        `json:"requiresAuthentication"`
				DueDate                time.Time   `json:"dueDate"`
				AvailablePayments      interface{} `json:"availablePayments"`
			} `json:"paymentSystems"`
			Payments []struct {
				PaymentSystem          string      `json:"paymentSystem"`
				Bin                    interface{} `json:"bin"`
				AccountID              interface{} `json:"accountId"`
				TokenID                interface{} `json:"tokenId"`
				Installments           int         `json:"installments"`
				ReferenceValue         int         `json:"referenceValue"`
				Value                  int         `json:"value"`
				MerchantSellerPayments []struct {
					ID               string `json:"id"`
					Installments     int    `json:"installments"`
					ReferenceValue   int    `json:"referenceValue"`
					Value            int    `json:"value"`
					InterestRate     int    `json:"interestRate"`
					InstallmentValue int    `json:"installmentValue"`
				} `json:"merchantSellerPayments"`
			} `json:"payments"`
			GiftCards         []interface{} `json:"giftCards"`
			GiftCardMessages  []interface{} `json:"giftCardMessages"`
			AvailableAccounts []interface{} `json:"availableAccounts"`
			AvailableTokens   []interface{} `json:"availableTokens"`
		} `json:"paymentData"`
		MarketingData interface{} `json:"marketingData"`
		Sellers       []struct {
			ID   string `json:"id"`
			Name string `json:"name"`
			Logo string `json:"logo"`
		} `json:"sellers"`
		ClientPreferencesData struct {
			Locale          interface{} `json:"locale"`
			OptinNewsLetter bool        `json:"optinNewsLetter"`
		} `json:"clientPreferencesData"`
		CommercialConditionData interface{} `json:"commercialConditionData"`
		StorePreferencesData    struct {
			CountryCode        string `json:"countryCode"`
			SaveUserData       bool   `json:"saveUserData"`
			TimeZone           string `json:"timeZone"`
			CurrencyCode       string `json:"currencyCode"`
			CurrencyLocale     int    `json:"currencyLocale"`
			CurrencySymbol     string `json:"currencySymbol"`
			CurrencyFormatInfo struct {
				CurrencyDecimalDigits    int    `json:"currencyDecimalDigits"`
				CurrencyDecimalSeparator string `json:"currencyDecimalSeparator"`
				CurrencyGroupSeparator   string `json:"currencyGroupSeparator"`
				CurrencyGroupSize        int    `json:"currencyGroupSize"`
				StartsWithCurrencySymbol bool   `json:"startsWithCurrencySymbol"`
			} `json:"currencyFormatInfo"`
		} `json:"storePreferencesData"`
		GiftRegistryData interface{} `json:"giftRegistryData"`
		OpenTextField    interface{} `json:"openTextField"`
		InvoiceData      interface{} `json:"invoiceData"`
		CustomData       interface{} `json:"customData"`
		ItemMetadata     struct {
			Items []struct {
				ID              string        `json:"id"`
				Seller          string        `json:"seller"`
				Name            string        `json:"name"`
				SkuName         string        `json:"skuName"`
				ProductID       string        `json:"productId"`
				RefID           string        `json:"refId"`
				Ean             string        `json:"ean"`
				ImageURL        string        `json:"imageUrl"`
				DetailURL       string        `json:"detailUrl"`
				AssemblyOptions []interface{} `json:"assemblyOptions"`
			} `json:"items"`
		} `json:"itemMetadata"`
		HooksData            interface{} `json:"hooksData"`
		RatesAndBenefitsData struct {
			RateAndBenefitsIdentifiers []interface{} `json:"rateAndBenefitsIdentifiers"`
			Teaser                     []interface{} `json:"teaser"`
		} `json:"ratesAndBenefitsData"`
		SubscriptionData interface{} `json:"subscriptionData"`
		ItemsOrdination  interface{} `json:"itemsOrdination"`
	} `json:"orderForm"`
	TransactionData interface{} `json:"transactionData"`
	Orders          interface{} `json:"orders"`
}

type OrderResponse struct {
	OrderForm       interface{} `json:"orderForm"`
	TransactionData TransactionData `json:"transactionData"`
	Orders []struct {
		OrderID                string      `json:"orderId"`
		OrderGroup             string      `json:"orderGroup"`
		State                  string      `json:"state"`
		IsCheckedIn            bool        `json:"isCheckedIn"`
		SellerOrderID          string      `json:"sellerOrderId"`
		StoreID                interface{} `json:"storeId"`
		CheckedInPickupPointID interface{} `json:"checkedInPickupPointId"`
		Value                  int         `json:"value"`
		Items                  []struct {
			UniqueID              string        `json:"uniqueId"`
			ID                    string        `json:"id"`
			ProductID             string        `json:"productId"`
			RefID                 string        `json:"refId"`
			Ean                   string        `json:"ean"`
			Name                  string        `json:"name"`
			SkuName               string        `json:"skuName"`
			ModalType             interface{}   `json:"modalType"`
			ParentItemIndex       interface{}   `json:"parentItemIndex"`
			ParentAssemblyBinding interface{}   `json:"parentAssemblyBinding"`
			Assemblies            []interface{} `json:"assemblies"`
			PriceValidUntil       time.Time     `json:"priceValidUntil"`
			Tax                   int           `json:"tax"`
			Price                 int           `json:"price"`
			ListPrice             int           `json:"listPrice"`
			ManualPrice           interface{}   `json:"manualPrice"`
			SellingPrice          int           `json:"sellingPrice"`
			RewardValue           int           `json:"rewardValue"`
			IsGift                bool          `json:"isGift"`
			AdditionalInfo        struct {
				BrandName      string      `json:"brandName"`
				BrandID        string      `json:"brandId"`
				OfferingInfo   interface{} `json:"offeringInfo"`
				OfferingType   interface{} `json:"offeringType"`
				OfferingTypeID interface{} `json:"offeringTypeId"`
			} `json:"additionalInfo"`
			PreSaleDate        interface{} `json:"preSaleDate"`
			ProductCategoryIds string      `json:"productCategoryIds"`
			ProductCategories  struct {
				Num13 string `json:"13"`
			} `json:"productCategories"`
			Quantity            int           `json:"quantity"`
			Seller              string        `json:"seller"`
			SellerChain         []string      `json:"sellerChain"`
			ImageURL            string        `json:"imageUrl"`
			DetailURL           string        `json:"detailUrl"`
			Components          []interface{} `json:"components"`
			BundleItems         []interface{} `json:"bundleItems"`
			Attachments         []interface{} `json:"attachments"`
			AttachmentOfferings []interface{} `json:"attachmentOfferings"`
			Offerings           []interface{} `json:"offerings"`
			PriceTags           []interface{} `json:"priceTags"`
			Availability        string        `json:"availability"`
			MeasurementUnit     string        `json:"measurementUnit"`
			UnitMultiplier      float64           `json:"unitMultiplier"`
		} `json:"items"`
		Sellers []struct {
			ID   string `json:"id"`
			Name string `json:"name"`
			Logo string `json:"logo"`
		} `json:"sellers"`
		Totals []struct {
			ID    string `json:"id"`
			Name  string `json:"name"`
			Value int    `json:"value"`
		} `json:"totals"`
		ClientProfileData struct {
			Email                    string      `json:"email"`
			FirstName                string      `json:"firstName"`
			LastName                 string      `json:"lastName"`
			Document                 string      `json:"document"`
			DocumentType             string      `json:"documentType"`
			Phone                    string      `json:"phone"`
			CorporateName            interface{} `json:"corporateName"`
			TradeName                interface{} `json:"tradeName"`
			CorporateDocument        interface{} `json:"corporateDocument"`
			StateInscription         interface{} `json:"stateInscription"`
			CorporatePhone           interface{} `json:"corporatePhone"`
			IsCorporate              bool        `json:"isCorporate"`
			ProfileCompleteOnLoading interface{} `json:"profileCompleteOnLoading"`
			ProfileErrorOnLoading    interface{} `json:"profileErrorOnLoading"`
			CustomerClass            interface{} `json:"customerClass"`
		} `json:"clientProfileData"`
		RatesAndBenefitsData struct {
			RateAndBenefitsIdentifiers []interface{} `json:"rateAndBenefitsIdentifiers"`
			Teaser                     []interface{} `json:"teaser"`
		} `json:"ratesAndBenefitsData"`
		ShippingData struct {
			Address struct {
				AddressType    string      `json:"addressType"`
				ReceiverName   string      `json:"receiverName"`
				AddressID      string      `json:"addressId"`
				PostalCode     string      `json:"postalCode"`
				City           string      `json:"city"`
				State          string      `json:"state"`
				Country        string      `json:"country"`
				Street         string      `json:"street"`
				Number         string      `json:"number"`
				Neighborhood   string      `json:"neighborhood"`
				Complement     string      `json:"complement"`
				Reference      interface{} `json:"reference"`
				GeoCoordinates []float64   `json:"geoCoordinates"`
			} `json:"address"`
			LogisticsInfo []struct {
				ItemIndex               int    `json:"itemIndex"`
				SelectedSLA             string `json:"selectedSla"`
				SelectedDeliveryChannel string `json:"selectedDeliveryChannel"`
				AddressID               string `json:"addressId"`
				Slas                    []struct {
					ID              string `json:"id"`
					DeliveryChannel string `json:"deliveryChannel"`
					Name            string `json:"name"`
					DeliveryIds     []struct {
						CourierID   string `json:"courierId"`
						WarehouseID string `json:"warehouseId"`
						DockID      string `json:"dockId"`
						CourierName string `json:"courierName"`
						Quantity    int    `json:"quantity"`
					} `json:"deliveryIds"`
					ShippingEstimate         string        `json:"shippingEstimate"`
					ShippingEstimateDate     interface{}   `json:"shippingEstimateDate"`
					LockTTL                  string        `json:"lockTTL"`
					AvailableDeliveryWindows []interface{} `json:"availableDeliveryWindows"`
					DeliveryWindow           interface{}   `json:"deliveryWindow"`
					Price                    int           `json:"price"`
					ListPrice                int           `json:"listPrice"`
					Tax                      int           `json:"tax"`
					PickupStoreInfo          struct {
						IsPickupStore  bool        `json:"isPickupStore"`
						FriendlyName   interface{} `json:"friendlyName"`
						Address        interface{} `json:"address"`
						AdditionalInfo interface{} `json:"additionalInfo"`
						DockID         interface{} `json:"dockId"`
					} `json:"pickupStoreInfo"`
					PickupPointID  interface{} `json:"pickupPointId"`
					PickupDistance float64         `json:"pickupDistance"`
					PolygonName    interface{} `json:"polygonName"`
				} `json:"slas"`
				ShipsTo          []string `json:"shipsTo"`
				ItemID           string   `json:"itemId"`
				DeliveryChannels []struct {
					ID string `json:"id"`
				} `json:"deliveryChannels"`
			} `json:"logisticsInfo"`
			SelectedAddresses []struct {
				AddressType    string      `json:"addressType"`
				ReceiverName   string      `json:"receiverName"`
				AddressID      string      `json:"addressId"`
				PostalCode     string      `json:"postalCode"`
				City           string      `json:"city"`
				State          string      `json:"state"`
				Country        string      `json:"country"`
				Street         string      `json:"street"`
				Number         string      `json:"number"`
				Neighborhood   string      `json:"neighborhood"`
				Complement     string      `json:"complement"`
				Reference      interface{} `json:"reference"`
				GeoCoordinates []float64   `json:"geoCoordinates"`
			} `json:"selectedAddresses"`
			AvailableAddresses []struct {
				AddressType    string      `json:"addressType"`
				ReceiverName   string      `json:"receiverName"`
				AddressID      string      `json:"addressId"`
				PostalCode     string      `json:"postalCode"`
				City           string      `json:"city"`
				State          string      `json:"state"`
				Country        string      `json:"country"`
				Street         string      `json:"street"`
				Number         string      `json:"number"`
				Neighborhood   string      `json:"neighborhood"`
				Complement     string      `json:"complement"`
				Reference      interface{} `json:"reference"`
				GeoCoordinates []float64   `json:"geoCoordinates"`
			} `json:"availableAddresses"`
			PickupPoints []struct {
				FriendlyName string `json:"friendlyName"`
				Address      struct {
					AddressType    string      `json:"addressType"`
					ReceiverName   interface{} `json:"receiverName"`
					AddressID      string      `json:"addressId"`
					PostalCode     string      `json:"postalCode"`
					City           string      `json:"city"`
					State          string      `json:"state"`
					Country        string      `json:"country"`
					Street         string      `json:"street"`
					Number         string      `json:"number"`
					Neighborhood   string      `json:"neighborhood"`
					Complement     string      `json:"complement"`
					Reference      interface{} `json:"reference"`
					GeoCoordinates []float64   `json:"geoCoordinates"`
				} `json:"address"`
				AdditionalInfo string `json:"additionalInfo"`
				ID             string `json:"id"`
				BusinessHours  []struct {
					DayOfWeek   int    `json:"DayOfWeek"`
					OpeningTime string `json:"OpeningTime"`
					ClosingTime string `json:"ClosingTime"`
				} `json:"businessHours"`
			} `json:"pickupPoints"`
		} `json:"shippingData"`
		PaymentData struct {
			GiftCards    []interface{} `json:"giftCards"`
			Transactions []struct {
				IsActive          bool          `json:"isActive"`
				TransactionID     string        `json:"transactionId"`
				MerchantName      string        `json:"merchantName"`
				Payments          []interface{} `json:"payments"`
				SharedTransaction bool          `json:"sharedTransaction"`
			} `json:"transactions"`
		} `json:"paymentData"`
		ClientPreferencesData   interface{} `json:"clientPreferencesData"`
		CommercialConditionData interface{} `json:"commercialConditionData"`
		GiftRegistryData        interface{} `json:"giftRegistryData"`
		MarketingData           interface{} `json:"marketingData"`
		StorePreferencesData    struct {
			CountryCode        string `json:"countryCode"`
			SaveUserData       bool   `json:"saveUserData"`
			TimeZone           string `json:"timeZone"`
			CurrencyCode       string `json:"currencyCode"`
			CurrencyLocale     int    `json:"currencyLocale"`
			CurrencySymbol     string `json:"currencySymbol"`
			CurrencyFormatInfo struct {
				CurrencyDecimalDigits    int    `json:"currencyDecimalDigits"`
				CurrencyDecimalSeparator string `json:"currencyDecimalSeparator"`
				CurrencyGroupSeparator   string `json:"currencyGroupSeparator"`
				CurrencyGroupSize        int    `json:"currencyGroupSize"`
				StartsWithCurrencySymbol bool   `json:"startsWithCurrencySymbol"`
			} `json:"currencyFormatInfo"`
		} `json:"storePreferencesData"`
		OpenTextField interface{} `json:"openTextField"`
		InvoiceData   interface{} `json:"invoiceData"`
		ItemMetadata  struct {
			Items []struct {
				ID              string        `json:"id"`
				Seller          string        `json:"seller"`
				Name            string        `json:"name"`
				SkuName         string        `json:"skuName"`
				ProductID       string        `json:"productId"`
				RefID           string        `json:"refId"`
				Ean             string        `json:"ean"`
				ImageURL        string        `json:"imageUrl"`
				DetailURL       string        `json:"detailUrl"`
				AssemblyOptions []interface{} `json:"assemblyOptions"`
			} `json:"items"`
		} `json:"itemMetadata"`
		TaxData              interface{} `json:"taxData"`
		CustomData           interface{} `json:"customData"`
		HooksData            interface{} `json:"hooksData"`
		ChangeData           interface{} `json:"changeData"`
		SubscriptionData     interface{} `json:"subscriptionData"`
		SalesChannel         string      `json:"salesChannel"`
		FollowUpEmail        string      `json:"followUpEmail"`
		CreationDate         time.Time   `json:"creationDate"`
		LastChange           time.Time   `json:"lastChange"`
		TimeZoneCreationDate string      `json:"timeZoneCreationDate"`
		TimeZoneLastChange   string      `json:"timeZoneLastChange"`
		IsCompleted          bool        `json:"isCompleted"`
		HostName             string      `json:"hostName"`
		MerchantName         interface{} `json:"merchantName"`
		UserType             string      `json:"userType"`
		RoundingError        int         `json:"roundingError"`
		AllowEdition         bool        `json:"allowEdition"`
		AllowCancellation    bool        `json:"allowCancellation"`
		IsUserDataVisible    bool        `json:"isUserDataVisible"`
		AllowChangeSeller    bool        `json:"allowChangeSeller"`
	} `json:"orders"`
}

type ApiCartSimulationRequest struct {
	Items []Item `json:"items"`
	PostalCode string `json:postalCode`
	Country string `json:country`
}

type ApiCartSimulationResponse struct {
	Items []struct {
		ID                    string        `json:"id"`
		RequestIndex          int           `json:"requestIndex"`
		Quantity              int           `json:"quantity"`
		Seller                string        `json:"seller"`
		MerchantName          interface{}   `json:"merchantName"`
		ParentItemIndex       interface{}   `json:"parentItemIndex"`
		ParentAssemblyBinding interface{}   `json:"parentAssemblyBinding"`
		PriceValidUntil       time.Time     `json:"priceValidUntil"`
		Price                 int           `json:"price"`
		ListPrice             int           `json:"listPrice"`
		Offerings             []interface{} `json:"offerings"`
		PriceTags             []interface{} `json:"priceTags"`
		MeasurementUnit       string        `json:"measurementUnit"`
		UnitMultiplier        float32           `json:"unitMultiplier"`
		AttachmentOfferings   []interface{} `json:"attachmentOfferings"`
	} `json:"items"`
	PostalCode     string        `json:"postalCode"`
	GeoCoordinates []interface{} `json:"geoCoordinates"`
	Country        string        `json:"country"`
	LogisticsInfo  []struct {
		ItemIndex               int         `json:"itemIndex"`
		AddressID               interface{} `json:"addressId"`
		SelectedSLA             interface{} `json:"selectedSla"`
		SelectedDeliveryChannel interface{} `json:"selectedDeliveryChannel"`
		StockBalance            int         `json:"stockBalance"`
		Quantity                int         `json:"quantity"`
		ShipsTo                 []string    `json:"shipsTo"`
		Slas                    []struct {
			ID              string `json:"id"`
			DeliveryChannel string `json:"deliveryChannel"`
			Name            string `json:"name"`
			DeliveryIds     []struct {
				CourierID   string `json:"courierId"`
				WarehouseID string `json:"warehouseId"`
				DockID      string `json:"dockId"`
				CourierName string `json:"courierName"`
				Quantity    int    `json:"quantity"`
			} `json:"deliveryIds"`
			ShippingEstimate         string        `json:"shippingEstimate"`
			DockEstimate             string        `json:"dockEstimate"`
			AvailableDeliveryWindows []interface{} `json:"availableDeliveryWindows"`
			Price                    int           `json:"price"`
			PickupStoreInfo          struct {
				IsPickupStore  bool        `json:"isPickupStore"`
				FriendlyName   interface{} `json:"friendlyName"`
				Address        interface{} `json:"address"`
				AdditionalInfo interface{} `json:"additionalInfo"`
				DockID         interface{} `json:"dockId"`
			} `json:"pickupStoreInfo"`
			PickupPointID  interface{} `json:"pickupPointId"`
			PickupDistance interface{} `json:"pickupDistance"`
			PolygonName    interface{} `json:"polygonName"`
		} `json:"slas"`
		DeliveryChannels []struct {
			ID           string `json:"id"`
			StockBalance int    `json:"stockBalance"`
		} `json:"deliveryChannels"`
	} `json:"logisticsInfo"`
	PickupPoints []interface{} `json:"pickupPoints"`
	Messages     []interface{} `json:"messages"`
	ItemMetadata interface{}   `json:"itemMetadata"`
}

type ApiOrderRequest struct {
	Order FinalOrder `json:"order"`
	PaymentInfo []PaymentInfo `json:"payments"`
}

type SuccessReponse struct {
	Message string `json:"message"`
	Order FinalReturnOrder `json:"order"`
}

type TestOrderResponse struct {
	Cart ApiCartSimulationResponse `json:cart`
	Order OrderResponse `json:order`
}

type ApiTestOrderRequest struct {
	Order Order `json:"order"`
	PaymentInfo []PaymentInfo `json:"payments"`
}

type FinalReturnOrder struct {
	OrderID                     string      `json:"orderId"`
	Sequence                    string      `json:"sequence"`
	MarketplaceOrderID          string      `json:"marketplaceOrderId"`
	MarketplaceServicesEndpoint string      `json:"marketplaceServicesEndpoint"`
	SellerOrderID               string      `json:"sellerOrderId"`
	Origin                      string      `json:"origin"`
	AffiliateID                 string      `json:"affiliateId"`
	SalesChannel                string      `json:"salesChannel"`
	MerchantName                interface{} `json:"merchantName"`
	Status                      string      `json:"status"`
	StatusDescription           string      `json:"statusDescription"`
	Value                       int         `json:"value"`
	CreationDate                time.Time   `json:"creationDate"`
	LastChange                  time.Time   `json:"lastChange"`
	OrderGroup                  string      `json:"orderGroup"`
	Totals                      []struct {
		ID    string `json:"id"`
		Name  string `json:"name"`
		Value int    `json:"value"`
	} `json:"totals"`
	Items []struct {
		UniqueID       string `json:"uniqueId"`
		ID             string `json:"id"`
		ProductID      string `json:"productId"`
		Ean            string `json:"ean"`
		LockID         string `json:"lockId"`
		ItemAttachment struct {
			Content struct {
			} `json:"content"`
			Name interface{} `json:"name"`
		} `json:"itemAttachment"`
		Attachments     []interface{} `json:"attachments"`
		Quantity        int           `json:"quantity"`
		Seller          string        `json:"seller"`
		Name            string        `json:"name"`
		RefID           string        `json:"refId"`
		Price           int           `json:"price"`
		ListPrice       int           `json:"listPrice"`
		ManualPrice     interface{}   `json:"manualPrice"`
		PriceTags       []interface{} `json:"priceTags"`
		ImageURL        string        `json:"imageUrl"`
		DetailURL       string        `json:"detailUrl"`
		Components      []interface{} `json:"components"`
		BundleItems     []interface{} `json:"bundleItems"`
		Params          []interface{} `json:"params"`
		Offerings       []interface{} `json:"offerings"`
		SellerSku       string        `json:"sellerSku"`
		PriceValidUntil interface{}   `json:"priceValidUntil"`
		Commission      int           `json:"commission"`
		Tax             int           `json:"tax"`
		PreSaleDate     interface{}   `json:"preSaleDate"`
		AdditionalInfo  struct {
			BrandName             string `json:"brandName"`
			BrandID               string `json:"brandId"`
			CategoriesIds         string `json:"categoriesIds"`
			ProductClusterID      string `json:"productClusterId"`
			CommercialConditionID string `json:"commercialConditionId"`
			Dimension             struct {
				Cubicweight float64 `json:"cubicweight"`
				Height      float64 `json:"height"`
				Length      float64 `json:"length"`
				Weight      float64 `json:"weight"`
				Width       float64 `json:"width"`
			} `json:"dimension"`
			OfferingInfo   interface{} `json:"offeringInfo"`
			OfferingType   interface{} `json:"offeringType"`
			OfferingTypeID interface{} `json:"offeringTypeId"`
		} `json:"additionalInfo"`
		MeasurementUnit       string      `json:"measurementUnit"`
		UnitMultiplier        float64     `json:"unitMultiplier"`
		SellingPrice          int         `json:"sellingPrice"`
		IsGift                bool        `json:"isGift"`
		ShippingPrice         interface{} `json:"shippingPrice"`
		RewardValue           int         `json:"rewardValue"`
		FreightCommission     int         `json:"freightCommission"`
		PriceDefinitions      interface{} `json:"priceDefinitions"`
		TaxCode               interface{} `json:"taxCode"`
		ParentItemIndex       interface{} `json:"parentItemIndex"`
		ParentAssemblyBinding interface{} `json:"parentAssemblyBinding"`
		CallCenterOperator    interface{} `json:"callCenterOperator"`
		SerialNumbers         interface{} `json:"serialNumbers"`
	} `json:"items"`
	MarketplaceItems  []interface{} `json:"marketplaceItems"`
	ClientProfileData struct {
		ID                string      `json:"id"`
		Email             string      `json:"email"`
		FirstName         string      `json:"firstName"`
		LastName          string      `json:"lastName"`
		DocumentType      string      `json:"documentType"`
		Document          string      `json:"document"`
		Phone             string      `json:"phone"`
		CorporateName     interface{} `json:"corporateName"`
		TradeName         interface{} `json:"tradeName"`
		CorporateDocument interface{} `json:"corporateDocument"`
		StateInscription  interface{} `json:"stateInscription"`
		CorporatePhone    interface{} `json:"corporatePhone"`
		IsCorporate       bool        `json:"isCorporate"`
		UserProfileID     string      `json:"userProfileId"`
		CustomerClass     interface{} `json:"customerClass"`
	} `json:"clientProfileData"`
	GiftRegistryData     interface{} `json:"giftRegistryData"`
	MarketingData        interface{} `json:"marketingData"`
	RatesAndBenefitsData struct {
		ID                         string        `json:"id"`
		RateAndBenefitsIdentifiers []interface{} `json:"rateAndBenefitsIdentifiers"`
	} `json:"ratesAndBenefitsData"`
	ShippingData struct {
		ID      string `json:"id"`
		Address struct {
			AddressType    string      `json:"addressType"`
			ReceiverName   string      `json:"receiverName"`
			AddressID      string      `json:"addressId"`
			PostalCode     string      `json:"postalCode"`
			City           string      `json:"city"`
			State          string      `json:"state"`
			Country        string      `json:"country"`
			Street         string      `json:"street"`
			Number         string      `json:"number"`
			Neighborhood   string      `json:"neighborhood"`
			Complement     string      `json:"complement"`
			Reference      interface{} `json:"reference"`
			GeoCoordinates []float64   `json:"geoCoordinates"`
		} `json:"address"`
		LogisticsInfo []struct {
			ItemIndex            int         `json:"itemIndex"`
			SelectedSLA          string      `json:"selectedSla"`
			LockTTL              string      `json:"lockTTL"`
			Price                int         `json:"price"`
			ListPrice            int         `json:"listPrice"`
			SellingPrice         int         `json:"sellingPrice"`
			DeliveryWindow       interface{} `json:"deliveryWindow"`
			DeliveryCompany      string      `json:"deliveryCompany"`
			ShippingEstimate     string      `json:"shippingEstimate"`
			ShippingEstimateDate interface{} `json:"shippingEstimateDate"`
			Slas                 []struct {
				ID               string      `json:"id"`
				Name             string      `json:"name"`
				ShippingEstimate string      `json:"shippingEstimate"`
				DeliveryWindow   interface{} `json:"deliveryWindow"`
				Price            int         `json:"price"`
				DeliveryChannel  string      `json:"deliveryChannel"`
				PickupStoreInfo  struct {
					AdditionalInfo interface{} `json:"additionalInfo"`
					Address        interface{} `json:"address"`
					DockID         interface{} `json:"dockId"`
					FriendlyName   interface{} `json:"friendlyName"`
					IsPickupStore  bool        `json:"isPickupStore"`
				} `json:"pickupStoreInfo"`
				PolygonName interface{} `json:"polygonName"`
				LockTTL     string      `json:"lockTTL"`
			} `json:"slas"`
			ShipsTo     []string `json:"shipsTo"`
			DeliveryIds []struct {
				CourierID   string `json:"courierId"`
				CourierName string `json:"courierName"`
				DockID      string `json:"dockId"`
				Quantity    int    `json:"quantity"`
				WarehouseID string `json:"warehouseId"`
			} `json:"deliveryIds"`
			DeliveryChannel string `json:"deliveryChannel"`
			PickupStoreInfo struct {
				AdditionalInfo interface{} `json:"additionalInfo"`
				Address        interface{} `json:"address"`
				DockID         interface{} `json:"dockId"`
				FriendlyName   interface{} `json:"friendlyName"`
				IsPickupStore  bool        `json:"isPickupStore"`
			} `json:"pickupStoreInfo"`
			AddressID   string      `json:"addressId"`
			PolygonName interface{} `json:"polygonName"`
		} `json:"logisticsInfo"`
		TrackingHints     interface{} `json:"trackingHints"`
		SelectedAddresses []struct {
			AddressID      string      `json:"addressId"`
			AddressType    string      `json:"addressType"`
			ReceiverName   string      `json:"receiverName"`
			Street         string      `json:"street"`
			Number         string      `json:"number"`
			Complement     string      `json:"complement"`
			Neighborhood   string      `json:"neighborhood"`
			PostalCode     string      `json:"postalCode"`
			City           string      `json:"city"`
			State          string      `json:"state"`
			Country        string      `json:"country"`
			Reference      interface{} `json:"reference"`
			GeoCoordinates []float64   `json:"geoCoordinates"`
		} `json:"selectedAddresses"`
	} `json:"shippingData"`
	PaymentData struct {
		Transactions []struct {
			IsActive      bool   `json:"isActive"`
			TransactionID string `json:"transactionId"`
			MerchantName  string `json:"merchantName"`
			Payments      []struct {
				ID                 string      `json:"id"`
				PaymentSystem      string      `json:"paymentSystem"`
				PaymentSystemName  string      `json:"paymentSystemName"`
				Value              int         `json:"value"`
				Installments       int         `json:"installments"`
				ReferenceValue     int         `json:"referenceValue"`
				CardHolder         interface{} `json:"cardHolder"`
				CardNumber         interface{} `json:"cardNumber"`
				FirstDigits        interface{} `json:"firstDigits"`
				LastDigits         interface{} `json:"lastDigits"`
				Cvv2               interface{} `json:"cvv2"`
				ExpireMonth        interface{} `json:"expireMonth"`
				ExpireYear         interface{} `json:"expireYear"`
				URL                string      `json:"url"`
				GiftCardID         interface{} `json:"giftCardId"`
				GiftCardName       interface{} `json:"giftCardName"`
				GiftCardCaption    interface{} `json:"giftCardCaption"`
				RedemptionCode     interface{} `json:"redemptionCode"`
				Group              string      `json:"group"`
				Tid                interface{} `json:"tid"`
				DueDate            interface{} `json:"dueDate"`
				ConnectorResponses struct {
				} `json:"connectorResponses"`
			} `json:"payments"`
		} `json:"transactions"`
	} `json:"paymentData"`
	PackageAttachment struct {
		Packages []interface{} `json:"packages"`
	} `json:"packageAttachment"`
	Sellers []struct {
		ID   string `json:"id"`
		Name string `json:"name"`
		Logo string `json:"logo"`
	} `json:"sellers"`
	CallCenterOperatorData  interface{} `json:"callCenterOperatorData"`
	FollowUpEmail           string      `json:"followUpEmail"`
	LastMessage             interface{} `json:"lastMessage"`
	Hostname                string      `json:"hostname"`
	InvoiceData             interface{} `json:"invoiceData"`
	ChangesAttachment       interface{} `json:"changesAttachment"`
	OpenTextField           interface{} `json:"openTextField"`
	RoundingError           int         `json:"roundingError"`
	OrderFormID             string      `json:"orderFormId"`
	CommercialConditionData interface{} `json:"commercialConditionData"`
	IsCompleted             bool        `json:"isCompleted"`
	CustomData              interface{} `json:"customData"`
	StorePreferencesData    struct {
		CountryCode        string `json:"countryCode"`
		CurrencyCode       string `json:"currencyCode"`
		CurrencyFormatInfo struct {
			CurrencyDecimalDigits    int    `json:"CurrencyDecimalDigits"`
			CurrencyDecimalSeparator string `json:"CurrencyDecimalSeparator"`
			CurrencyGroupSeparator   string `json:"CurrencyGroupSeparator"`
			CurrencyGroupSize        int    `json:"CurrencyGroupSize"`
			StartsWithCurrencySymbol bool   `json:"StartsWithCurrencySymbol"`
		} `json:"currencyFormatInfo"`
		CurrencyLocale int    `json:"currencyLocale"`
		CurrencySymbol string `json:"currencySymbol"`
		TimeZone       string `json:"timeZone"`
	} `json:"storePreferencesData"`
	AllowCancellation bool `json:"allowCancellation"`
	AllowEdition      bool `json:"allowEdition"`
	IsCheckedIn       bool `json:"isCheckedIn"`
	Marketplace       struct {
		BaseURL     string      `json:"baseURL"`
		IsCertified interface{} `json:"isCertified"`
		Name        string      `json:"name"`
	} `json:"marketplace"`
	AuthorizedDate interface{} `json:"authorizedDate"`
	InvoicedDate   interface{} `json:"invoicedDate"`
	CancelReason   interface{} `json:"cancelReason"`
	ItemMetadata   struct {
		Items []struct {
			ID              string        `json:"Id"`
			Seller          string        `json:"Seller"`
			Name            string        `json:"Name"`
			SkuName         string        `json:"SkuName"`
			ProductID       string        `json:"ProductId"`
			RefID           string        `json:"RefId"`
			Ean             string        `json:"Ean"`
			ImageURL        string        `json:"ImageUrl"`
			DetailURL       string        `json:"DetailUrl"`
			AssemblyOptions []interface{} `json:"AssemblyOptions"`
		} `json:"Items"`
	} `json:"itemMetadata"`
}

type CustomerProfile struct {
	UserProfileID     string `json:"userProfileId"`
	ProfileProvider   string `json:"profileProvider"`
	AvailableAccounts []struct {
		AccountID          string   `json:"accountId"`
		PaymentSystem      string   `json:"paymentSystem"`
		PaymentSystemName  string   `json:"paymentSystemName"`
		CardNumber         string   `json:"cardNumber"`
		Bin                string   `json:"bin"`
		AvailableAddresses []string `json:"availableAddresses"`
	} `json:"availableAccounts"`
	AvailableAddresses []struct {
		AddressType    string      `json:"addressType"`
		ReceiverName   string      `json:"receiverName"`
		AddressID      string      `json:"addressId"`
		PostalCode     string      `json:"postalCode"`
		City           string      `json:"city"`
		State          string      `json:"state"`
		Country        string      `json:"country"`
		Street         string      `json:"street"`
		Number         string      `json:"number"`
		Neighborhood   string      `json:"neighborhood"`
		Complement     string      `json:"complement"`
		Reference      interface{} `json:"reference"`
		GeoCoordinates []float64   `json:"geoCoordinates"`
	} `json:"availableAddresses"`
	UserProfile struct {
		Email                    string      `json:"email"`
		FirstName                string      `json:"firstName"`
		LastName                 string      `json:"lastName"`
		Document                 string      `json:"document"`
		DocumentType             string      `json:"documentType"`
		Phone                    string      `json:"phone"`
		CorporateName            interface{} `json:"corporateName"`
		TradeName                interface{} `json:"tradeName"`
		CorporateDocument        interface{} `json:"corporateDocument"`
		StateInscription         interface{} `json:"stateInscription"`
		CorporatePhone           interface{} `json:"corporatePhone"`
		IsCorporate              bool        `json:"isCorporate"`
		ProfileCompleteOnLoading interface{} `json:"profileCompleteOnLoading"`
		ProfileErrorOnLoading    interface{} `json:"profileErrorOnLoading"`
		CustomerClass            interface{} `json:"customerClass"`
	} `json:"userProfile"`
}
