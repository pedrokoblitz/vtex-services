
type PaymentInfo struct {
	PaymentSystem            string    `json:"paymentSystem"`
	PaymentSystemName        string `json:"paymentSystemName"`
	Group                    string `json:"group"`
	Installments             int    `json:"installments"`
	InstallmentsInterestRate int    `json:"installmentsInterestRate"`
	InstallmentsValue        int    `json:"installmentsValue"`
	Value                    int    `json:"value"`
	ReferenceValue           int    `json:"referenceValue"`
	Fields                   CcField `json:"fields"`
	Transaction Transaction `json:"transaction"`
	CurrencyCode string `json:"currencyCode"`
}
