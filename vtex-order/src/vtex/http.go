package vtex

import (
	"log"
	"net/http"
	"strings"
)

func apiPostCartSimulation(payload []byte) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/fulfillment/pvt/orderForms/simulation"
	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("POST", url, reqPayload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiGetProfile(email string) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/profiles?email=" + email
	req, err = http.NewRequest("GET", url, nil)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiPutOrder(payload []byte) *http.Response {
	var req *http.Request
	var err error
	var url string

	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url = "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/orders"
	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("PUT", url, reqPayload)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}
	return res
}


func apiPostPayment(transactionID string, payload []byte) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "https://designupliving.vtexpayments.com.br/api/pub/transactions/" + transactionID + "/payments"

	reqPayload := strings.NewReader(string(payload))
	req, err = http.NewRequest("POST", url, reqPayload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiPostProcessPayment(orderGroup string, cookie *http.Cookie) *http.Response {
	var req *http.Request
	var err error
	var url string

	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url = "http://designupliving.vtexcommercestable.com.br/api/checkout/pub/gatewayCallback/" + orderGroup
	req, err = http.NewRequest("POST", url, nil)

	req.AddCookie(cookie)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}

func apiGetOrder(orderID string) *http.Response {
	var req *http.Request
	var err error
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	url := "http://designupliving.vtexcommercestable.com.br/api/oms/pvt/orders/" + orderID
	req, err = http.NewRequest("GET", url, nil)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("X-VTEX-API-AppKey", key)
	req.Header.Add("X-VTEX-API-AppToken", token)

	res, err := client.Do(req)

	if err != nil {
		log.Println(err)
	}

	return res
}
