package vtex

import (
	// "fmt"
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	// "strconv"
	"io/ioutil"
	// "time"
	// "github.com/davecgh/go-spew/spew"
	"github.com/jinzhu/copier"
	"strings"
)

func PutCreateOrder(w http.ResponseWriter, r *http.Request) {
	var res *http.Response
	var payload []byte

	var apiOrderRequest ApiOrderRequest
	var apiCart ApiCartSimulationRequest
	var vOrder OrderResponse

	var returnOrder FinalReturnOrder

	var err error

	// get store request
	payload, err = ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	// parse store request order
	err = json.Unmarshal(payload, &apiOrderRequest)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	email := apiOrderRequest.Order.ClientProfileData.Email
	apiCart.Items = apiOrderRequest.Order.Items
	apiCart.PostalCode = apiOrderRequest.Order.ShippingData.Address.PostalCode
	apiCart.Country = apiOrderRequest.Order.ShippingData.Address.Country

	// prepare api request
	payload, err = json.Marshal(apiCart)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	// send cart api request
	res = apiPostCartSimulation(payload)
	payload, err = ioutil.ReadAll(res.Body)
	if err != nil {
		// log.Println(err)
	}
	res.Body.Close()
	log.Println("CART SIMULATION", res.StatusCode)

	if res.StatusCode != 200 {
		http.Error(w, string(payload), res.StatusCode)
		return
	}

	// send profile api request
	res = apiGetProfile(email)
	payload, err = ioutil.ReadAll(res.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	res.Body.Close()
	// log.Println("PROFILE", string(payload))
	log.Println("PROFILE", res.StatusCode)

	if res.StatusCode != 200 {
		http.Error(w, string(payload), res.StatusCode)
		return
	}

	log.Println("COUPON", apiOrderRequest.Order.MarketingData.Coupon)
	// if not coupon copy order to diff struct
	if apiOrderRequest.Order.MarketingData.Coupon == "" {
		couponOrder := Order{}
		copier.Copy(&couponOrder, &apiOrderRequest.Order)
		payload, err = json.Marshal(couponOrder)
	} else {
		payload, err = json.Marshal(apiOrderRequest.Order)
	}


	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	// log.Println("ORDER PAYLOAD", string(payload))

	// send order request
	res = apiPutOrder(payload)
	payload, err = ioutil.ReadAll(res.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	res.Body.Close()

	log.Println("ORDER", res.StatusCode)
	log.Println(string(payload))

	if res.StatusCode != 201 {
		http.Error(w, string(payload), res.StatusCode)
		return
	}

	err = json.Unmarshal(payload, &vOrder)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	var chkoAuth *http.Cookie
	for _, cookie := range res.Cookies() {
		if cookie.Name == "Vtex_CHKO_Auth" {
			chkoAuth = cookie
		}
	}

	transactionID := vOrder.TransactionData.MerchantTransactions[0].TransactionID

	orderID := vOrder.Orders[0].OrderID
	orderGroups := strings.Split(orderID, "-")
	orderGroup := orderGroups[0]

	transactionID = vOrder.TransactionData.MerchantTransactions[0].TransactionID
	confirmation := apiOrderRequest.PaymentInfo
	confirmation[0].Transaction.ID = transactionID

	payload, err = json.Marshal(confirmation)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	res = apiPostPayment(transactionID, payload)
	payload, err = ioutil.ReadAll(res.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	res.Body.Close()

	log.Println("PAYMENT", res.StatusCode)

	if res.StatusCode != 201 {
		http.Error(w, string(payload), res.StatusCode)
		return
	}

	res = apiPostProcessPayment(orderGroup, chkoAuth)
	payload, err = ioutil.ReadAll(res.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	res.Body.Close()

	log.Println("PAYMENT PROCESSING", res.StatusCode)

	if res.StatusCode != 204 {
		http.Error(w, string(payload), res.StatusCode)
		return
	}

	res = apiGetOrder(orderID)
	payload, err = ioutil.ReadAll(res.Body)
	if err != nil {
		// log.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}
	res.Body.Close()

	err = json.Unmarshal(payload, &returnOrder)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	var s SuccessReponse
	s.Message = "Sucesso!"
	s.Order = returnOrder
	payload, err = json.Marshal(s)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}

func PostCartSimulation(w http.ResponseWriter, r *http.Request) {
	var payload []byte
	var res *http.Response
	var err error

	payload, err = ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	res = apiPostCartSimulation(payload)
	log.Println("CART SIMULATION", res.StatusCode)

	payload, err = ioutil.ReadAll(res.Body)
	if err != nil {
		// log.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}
	res.Body.Close()


	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}


func PutTestOrder(w http.ResponseWriter, r *http.Request) {
	var res *http.Response
	var payload []byte
	var vErrorOrder OrderResponse
	var apiOrderRequest ApiTestOrderRequest
	var apiCart ApiCartSimulationRequest
	var vCart ApiCartSimulationResponse
	var finalResponse TestOrderResponse
	var err error

	payload, err = ioutil.ReadAll(r.Body)

	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	// log.Println("ORDER PAYLOAD", string(payload))

	err = json.Unmarshal(payload, &apiOrderRequest)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	apiCart.Items = apiOrderRequest.Order.Items
	apiCart.PostalCode = apiOrderRequest.Order.ShippingData.Address.PostalCode
	apiCart.Country = apiOrderRequest.Order.ShippingData.Address.Country
	payload, err = json.Marshal(apiCart)
	if err != nil {
		// log.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}

	res = apiPostCartSimulation(payload)
	payload, err = ioutil.ReadAll(res.Body)
	if err != nil {
		// log.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}

	log.Println("CART SIMULATION", res.StatusCode)

	res.Body.Close()
	// log.Println("CART SIMULATION", res.StatusCode)
	// log.Println(string(payload))

	if res.StatusCode != 200 {
		// log.Println(string(payload))
		http.Error(w, string(payload), res.StatusCode)
		return
	}
	err = json.Unmarshal(payload, &vCart)
	if err != nil {
		// log.Println(err)
		http.Error(w, err.Error(), 500)
		// return
	}

	payload, err = json.Marshal(apiOrderRequest.Order)
	if err != nil {
		// log.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}

	// log.Println("COUPON", apiOrderRequest.Order.MarketingData.Coupon)

	res = apiPutOrder(payload)
	payload, err = ioutil.ReadAll(res.Body)
	if err != nil {
		// log.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}

	res.Body.Close()
	// log.Println("PLACE ORDER", res.StatusCode)
	// log.Println(string(payload))
	log.Println("ORDER", res.StatusCode)

	log.Println("PAYLOAD", string(payload))
	if res.StatusCode == 400 {
		// log.Println(string(payload))
		err = json.Unmarshal(payload, &vErrorOrder)
		if err != nil {
			// log.Println(err)
			http.Error(w, err.Error(), 500)
			// return
		}

		finalResponse.Order = vErrorOrder
		finalResponse.Cart = vCart
		payload, err = json.Marshal(finalResponse)
		if err != nil {
			// log.Println(err)
			http.Error(w, err.Error(), 500)
			// return
		}

		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		w.Write(payload)
	}
}

func GetOrder(w http.ResponseWriter, r *http.Request) {
	var res *http.Response
	var payload []byte
	var err error

	params := mux.Vars(r)
	orderID := params["orderId"]

	res = apiGetOrder(orderID)
	payload, err = ioutil.ReadAll(res.Body)
	if err != nil {
		// log.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}

	res.Body.Close()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}

func GetProfile(w http.ResponseWriter, r *http.Request) {
	var res *http.Response
	var payload []byte
	var err error

	params := mux.Vars(r)
	email := params["email"]

	res = apiGetProfile(email)
	payload, err = ioutil.ReadAll(res.Body)
	if err != nil {
		// log.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}

	res.Body.Close()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}
