package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"vtex"
)

func main() {
	r := mux.NewRouter()

    r.HandleFunc("/freight", vtex.PutTestOrder).Methods("POST")
    r.HandleFunc("/cart/sim", vtex.PostCartSimulation).Methods("POST")
    r.HandleFunc("/order", vtex.PutCreateOrder).Methods("PUT")
    r.HandleFunc("/order/{orderId}", vtex.GetOrder).Methods("GET")
    r.HandleFunc("/profile/{email}", vtex.GetProfile).Methods("GET")

	fmt.Println("Listening on 8081")
	log.Fatal(http.ListenAndServe(":8081", r))
}
